<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Task;

use App\Library\Task\TaskBase;
use Clue\React\Redis\Client;
use Clue\React\Redis\Factory;
use PHPSocketIO\SocketIO;
use Workerman\Worker;

/**
 * Class WsTask
 *
 * Console de gestion du websocket
 *
 * @package App\Task
 */
class WsTask extends TaskBase
{
    /**
     * Default way
     */
    public function mainAction()
    {
        self::nl('WebSocket de communication client/serveur,');
        self::nl("Voici la liste des commandes disponibles :");

        self::listMethods($this);
    }

    /**
     * Méthode de gestion du serveur Ws
     *
     * Gère le processus du Websocket
     */
    public function serverAction()
    {
        global $argv;

        /** @var \Phalcon\Config $config */
        $config = $this->di->get('config')->get('redis');

        // Efface les arguments nécessaires a l'accès de cette méthode pour la suite du script
        $argv = array_slice($argv, 2);

        // Déclaration du SocketIO
        $io = new SocketIO(2021);

        // Démarrage de l'écoute sur le socket
        $io->on('workerStart', function () use ($io, $config) {

            // Déclaration du travailleur
            $loop = Worker::getEventLoop();

            /** @noinspection PhpParamsInspection */
            $factory = new Factory($loop);

            $uri = ($config->get('host') ?? 'localhost') . ':' . ($config->get('port') ?? 6379);

            // Event vers la base redis
            $factory->createClient($uri)->then(function (Client $redis) use ($io) {

                /** @noinspection PhpUndefinedFieldInspection */
                $io->redis = $redis;

                // Ecoute les events pub/sub Redis et emit vers le socket
                $redis->on('message', function ($channel, $message) use ($io) {

                    self::nl("- emit:$channel|message:$message");
                    $io->emit($channel, $message);
                });
            });

            $io->on('connection', function ($socket) use ($io) {

                /** @noinspection PhpUndefinedMethodInspection */
                $socket->on('subscriber', function ($payload) use ($io) {

                    $data = json_decode($payload, true);

                    /** @noinspection PhpUndefinedFieldInspection */
                    if ($io->redis->get($data->refreshToken)) {

                        /** @noinspection PhpUndefinedFieldInspection */
                        $io->redis->subscribe($data->channel)->then(function () use ($data) {

                            self::nl("- subscribe:$data->channel");
                        });

                        $io->emit('subscriber', false);
                    }

                    $io->emit('subscriber', true);
                });
            });
        });

        // Démarre le processus
        Worker::runAll();
    }



//    /** @noinspection PhpUndefinedMethodInspection */
//$redis->subscribe('refresh')->then(function () {
//    echo '- subscribe:refresh' . PHP_EOL;
//});
//
//    /** @noinspection PhpUndefinedMethodInspection */
//$redis->subscribe('chat')->then(function () {
//    echo '- subscribe:chat' . PHP_EOL;
//});
}
