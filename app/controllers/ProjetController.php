<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\Projet;
use App\Model\Client;
use App\Model\Utilisateur;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class ProjetController
 * @package App\Controller
 *
 * @apiDefine DataProjetApi
 * @apiParam {Number} id_client Identifiant du client lié au projet
 * @apiParam {Number} id_utilisateur Identifiant de l'utilisateur du projet
 * @apiParam {String} nom Nom du projet
 */
class ProjetController extends ControllerApi
{
    /** @var string $model */
    protected $model = Projet::class;

    /** @var array $columns */
    protected $columns = ['id', 'id_client', 'id_utilisateur', 'reference', 'date_creation', 'nom'];

    /** @var array $join */
    protected $join = [
        [
            'class' => Client::class,
            'columns' => [
                'nom' => 'client_nom',
            ]
        ],
        [
            'class' => Utilisateur::class,
            'columns' => [
                'nom' => 'utilisateur_nom',
            ]
        ]
    ];

    /**
     * @inheritdoc
     *
     * @param ModelBase|Projet $model
     */
    protected function fetchSearch($model): array
    {
        $result = parent::fetchSearch($model);

        /** @var Client $client */
        $client = $model->getClient();

        /** @var Utilisateur $utilisateur */
        $utilisateur = $model->getUtilisateur();

        $result['client_nom_prenom'] = $client->getNom() . ' ' . $client->getPrenom();
        $result['utilisateur_nom_prenom'] = $utilisateur->getNom() . ' ' . $utilisateur->getPrenom();

        return $result;
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Projet $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setIdClient($this->request->getPost('id_client', Filter::FILTER_ABSINT, $model->getIdClient()));
        $model->setIdUtilisateur($this->request->getPost('id_utilisateur', Filter::FILTER_ABSINT, $model->getIdUtilisateur()));
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchProjetApi
     * @apiGroup ProjetApi
     * @apiVersion 0.0.1
     * @api {post} /projet/search Recherche des éléments
     * @apiPermission {role} | Projet | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateProjetApi
     * @apiGroup ProjetApi
     * @apiVersion 0.0.1
     * @api {post} /projet Ajoute un nouvel élément
     * @apiPermission {role} | Projet | create
     * @apiUse CreateGenericApi
     * @apiUse DataProjetApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadProjetApi
     * @apiGroup ProjetApi
     * @apiVersion 0.0.1
     * @api {get} /projet/:id Affiche un élément
     * @apiPermission {role} | Projet | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateProjetApi
     * @apiGroup ProjetApi
     * @apiVersion 0.0.1
     * @api {post} /projet/:id Modifie un élément
     * @apiPermission {role} | Projet | update
     * @apiUse UpdateGenericApi
     * @apiUse DataProjetApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteProjetApi
     * @apiGroup ProjetApi
     * @apiVersion 0.0.1
     * @api {delete} /projet/:id Supprime un élément
     * @apiPermission {role} | Projet | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
