<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerSecurity;
use App\Library\Helper;
use App\Model\AccessList;
use App\Model\ResourcesAccesses;
use App\Model\Roles;
use Phalcon\Acl\AdapterInterface;
use Phalcon\Acl\Role;
use Phalcon\Filter;

/**
 * Class AclController
 * @package App\Controller
 */
class AclController extends ControllerSecurity
{
    /** @var AdapterInterface $acl */
    protected $acl;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        $this->acl = $this->getDI()->getShared('acl');
    }

    /**
     * Méthode de modification des droits d'un rôle
     *
     * @param null $roleName
     * @param $resourceDisplay
     * @param $accessDisplay
     * @return array
     */
    public function accessAction($roleName, $resourceDisplay = null, $accessDisplay = null)
    {
        if ($this->request->isPost()) {

//            $roleName = $this->request->getPost('role', [Filter::FILTER_ALPHANUM, Filter::FILTER_TRIM]);
            $resourceName = $this->request->getPost('resource', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM]);
            $access = $this->request->getPost('access', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM]);
            $grant = $this->request->getPost('grant', Filter::FILTER_INT_CAST);

            $messagePart = null;

            switch ($grant) {
                case 0:
                    $messagePart = "est maintenant interdit d'utiliser la ressource";
                    $this->acl->deny($roleName, $resourceName, $access);
                    break;

                case 1:
                    $messagePart = 'peu maintenant utiliser la ressource';
                    $this->acl->allow($roleName, $resourceName, $access);
                    break;

                default:

                    $this->response->setStatusCode(409, 'Conflict');

                    return [
                        'status' => 'ERROR',
                        'message' => 'Le grant est inconnu !'
                    ];
            }

            $this->response->setStatusCode(201, 'Created');

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'message' => 'Le rôle ' . $roleName . ' ' . $messagePart . ' ' . $resourceName
            ];

        } elseif ($this->request->isDelete()
            && isset($roleName)
            && isset($resourceDisplay)
            && isset($accessDisplay)) {

            /** @var AccessList $access */
            $access = AccessList::findFirst(
                [
                    'conditions' => 'roles_name = ?1 AND resources_name = ?2 AND access_name = ?3',
                    'bind' => [
                        1 => $roleName,
                        2 => $resourceDisplay,
                        3 => $accessDisplay
                    ]
                ]
            );

            if ($access !== false) {

                if ($access->delete()) {

                    $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                    return [
                        'status' => 'SUCCESS',
                        'latency' => round((microtime(true) - $requestTime) * 1000),
                        'message' => "La ressource nommé " . $access->roles_name . '-' . $access->resources_name . '-' . $access->access_name . " à été supprimée !"
                    ];
                }

                $this->response->setStatusCode(409, 'Conflict');

                return [
                    'status' => 'FAILED',
                    'messages' => Helper::getMessages($access)
                ];
            }

            $this->response->setStatusCode(404, 'Not Found');

            return [
                'status' => 'ERROR',
                'message' => "La ressource demandée n'existe pas !"
            ];

        } else {

            /** @var AccessList $accesses */
            $accesses = AccessList::find(
                [
                    'conditions' => 'roles_name = ?1',
                    'bind' => [
                        1 => $roleName
                    ]
                ]
            );

            $access = [];

            /** @var AccessList $a */
            foreach ($accesses as $a) {

                $access[] = [
                    'resources_name' => $a->resources_name,
                    'access_name' => $a->access_name,
                    'allowed' => boolval(intval($a->allowed)),
                ];
            }

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $access
            ];
        }
    }

    public function inheritAction()
    {
        // TODO implémenter l'héritage
    }

    public function resourcesAction()
    {
        $resources = [];

        foreach ($this->acl->getResources() as $resource) {

            $resources[] = [
                'name' => $resource->getName(),
                'description' => $resource->getDescription()
            ];
        }

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'SUCCESS',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => $resources
        ];
    }

    public function accessesAction($resourcesName)
    {
        /** @var ResourcesAccesses[] $resourcesAccess */
        $resourcesAccess = ResourcesAccesses::find(
            [
                'conditions' => 'resources_name = ?1',
                'bind' => [
                    1 => $resourcesName
                ]
            ]
        );

        if ($resourcesAccess !== false) {

            $accesses = [];

            foreach ($resourcesAccess as $ra) {

                $accesses[] = [
                    'resource' => $ra->resources_name,
                    'access' => $ra->access_name
                ];
            }

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $accesses
            ];
        }

        return [];
    }

    /**
     * Méthode de gestion des rôles
     *
     * @param null $roleName
     * @return array
     */
    public function rolesAction($roleName = null)
    {
        if ($this->request->isPost() && empty($roleName)) {

            $role = new Role(
                ucfirst($this->request->getPost('name', [Filter::FILTER_ALPHANUM, Filter::FILTER_TRIM])),
                $this->request->getPost('description', Filter::FILTER_STRIPTAGS)
            );

            $this->acl->addRole($role);

            $roles = [];

            foreach ($this->acl->getRoles() as $r) {

                $roles[] = [
                    'name' => $r->getName(),
                    'description' => $r->getDescription(),
                ];
            }

            $this->response->setStatusCode(201, 'Created');

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'data' => [
                    'name' => $role->getName(),
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'description' => $role->getDescription(),
                    'list' => $roles
                ]
            ];

        } elseif ($this->request->isDelete() && isset($roleName)) {

            /** @var Roles $role */
            $role = Roles::findFirst(
                [
                    'conditions' => 'name = ?1',
                    'bind' => [
                        1 => $roleName
                    ]
                ]
            );

            if ($role !== false) {

                if ($role->delete()) {

                    $roles = [];

                    foreach ($this->acl->getRoles() as $role) {

                        $roles[] = [
                            'name' => $role->getName(),
                            'description' => $role->getDescription(),
                        ];
                    }

                    $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                    return [
                        'status' => 'SUCCESS',
                        'latency' => round((microtime(true) - $requestTime) * 1000),
                        'message' => "La ressource nommé " . $role->getName() . " à été supprimée !",
                        'list' => $roles
                    ];
                }

                $this->response->setStatusCode(409, 'Conflict');

                return [
                    'status' => 'FAILED',
                    'messages' => Helper::getMessages($role)
                ];
            }

            $this->response->setStatusCode(404, 'Not Found');

            return [
                'status' => 'ERROR',
                'message' => "La ressource demandée n'existe pas !"
            ];

        } elseif ($this->request->isGet() && isset($roleName)) {

            /** @var Roles $role */
            $role = Roles::findFirst(
                [
                    'conditions' => 'name = ?1',
                    'bind' => [
                        1 => $roleName
                    ]
                ]
            );

            if ($role !== false) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'data' => $role->toArray()
                ];
            }

        } else {

            $roles = [];

            foreach ($this->acl->getRoles() as $role) {

                $roles[] = [
                    'name' => $role->getName(),
                    'description' => $role->getDescription(),
                ];
            }

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $roles
            ];
        }

        return [];
    }
}
