<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\NatureModule;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class NatureModuleController
 * @package App\Controller
 *
 * @apiDefine DataNatureModuleApi
 * @apiParam {Stribg} nom Nom de la nature du module
 * @apiParam {Float} hauteur Hauteur de la nature du module
 * @apiParam {Float} longueur Longueur de la nature du module
 * @apiParam {String} unite Unité de la nature du module
 */
class NatureModuleController extends ControllerApi
{
    /** @var string $model */
    protected $model = NatureModule::class;

    /** @var array $columns */
    protected $columns = ['id', 'nom', 'hauteur', 'longueur', 'unite'];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|NatureModule $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setHauteur($this->request->getPost('hauteur', Filter::FILTER_FLOAT_CAST, $model->getHauteur()));
        $model->setLongueur($this->request->getPost('longueur', Filter::FILTER_FLOAT_CAST, $model->getLongueur()));
        $model->setUnite($this->request->getPost('unite', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getUnite()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchNatureModuleApi
     * @apiGroup NatureModuleApi
     * @apiVersion 0.0.1
     * @api {post} /nature-module/search Recherche des éléments
     * @apiPermission {role} | NatureModule | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateNatureModuleApi
     * @apiGroup NatureModuleApi
     * @apiVersion 0.0.1
     * @api {post} /nature-module Ajoute un nouvel élément
     * @apiPermission {role} | NatureModule | create
     * @apiUse CreateGenericApi
     * @apiUse DataNatureModuleApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadNatureModuleApi
     * @apiGroup NatureModuleApi
     * @apiVersion 0.0.1
     * @api {get} /nature-module/:id Affiche un élément
     * @apiPermission {role} | NatureModule | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateNatureModuleApi
     * @apiGroup NatureModuleApi
     * @apiVersion 0.0.1
     * @api {post} /nature-module/:id Modifie un élément
     * @apiPermission {role} | NatureModule | update
     * @apiUse UpdateGenericApi
     * @apiUse DataNatureModuleApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteNatureModuleApi
     * @apiGroup NatureModuleApi
     * @apiVersion 0.0.1
     * @api {delete} /nature-module/:id Supprime un élément
     * @apiPermission {role} | NatureModule | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
