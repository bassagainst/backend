<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\Composant;
use App\Model\FamilleComposant;
use App\Model\Fournisseur;
use App\Model\Tva;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class ComposantController
 * @package App\Controller
 *
 * @apiDefine DataComposantApi
 * @apiParam {Number} id_famille_composant Identifiant de la famille de composant du composant
 * @apiParam {Number} id_fournisseur Identifiant du fournisseur du composant
 * @apiParam {Number} id_tva Identifiant de la tva du composant
 * @apiParam {Number} colisage Identifiant du colisage du composant
 * @apiParam {String} nom Nom du composant
 * @apiParam {Float} prix_ht Prix hors taxes du composant
 * @apiParam {Float} marge MArge du composant
 * @apiParam {String} description Description du composant
 * @apiParam {String} unite_colisage Unité de colisage du composant
 */
class ComposantController extends ControllerApi
{
    /** @var string $model */
    protected $model = Composant::class;

    /** @var array $columns */
    protected $columns = ['id', 'id_fournisseur', 'id_famille_composant', 'id_tva',
        'colisage', 'nom', 'prix_ht', 'marge', 'description', 'unite_colisage'];

    /** @var array $join */
    protected $join = [
        [
            'class' => FamilleComposant::class,
            'columns' => [
                'label' => 'famille_composant_nom'
            ]
        ],
        [
            'class' => Tva::class,
            'columns' => [
                'label' => 'tva_nom'
            ]
        ],
        [
            'class' => Fournisseur::class,
            'columns' => [
                'nom' => 'fournisseur_nom'
            ]
        ]
    ];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Composant $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setIdFamilleComposant($this->request->getPost('id_famille_composant', Filter::FILTER_ABSINT, $model->getIdFamilleComposant()));
        $model->setIdFournisseur($this->request->getPost('id_fournisseur', Filter::FILTER_ABSINT, $model->getIdFournisseur()));
        $model->setIdTva($this->request->getPost('id_tva', Filter::FILTER_ABSINT, $model->getIdTva()));
        $model->setColisage($this->request->getPost('colisage', Filter::FILTER_ABSINT, $model->getColisage()));
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setPrixHt($this->request->getPost('prix_ht', Filter::FILTER_FLOAT_CAST, $model->getPrixHt()));
        $model->setMarge($this->request->getPost('marge', Filter::FILTER_FLOAT_CAST, $model->getMarge()));
        $model->setDescription($this->request->getPost('description', Filter::FILTER_STRIPTAGS, $model->getDescription()));
        $model->setUniteColisage($this->request->getPost('unite_colisage', Filter::FILTER_TRIM, $model->getUniteColisage()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchComposantApi
     * @apiGroup ComposantApi
     * @apiVersion 0.0.1
     * @api {post} /composant/search Recherche des éléments
     * @apiPermission {role} | Composant | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateComposantApi
     * @apiGroup ComposantApi
     * @apiVersion 0.0.1
     * @api {post} /composant Ajoute un nouvel élément
     * @apiPermission {role} | Composant | create
     * @apiUse CreateGenericApi
     * @apiUse DataComposantApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadComposantApi
     * @apiGroup ComposantApi
     * @apiVersion 0.0.1
     * @api {get} /composant/:id Affiche un élément
     * @apiPermission {role} | Composant | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateComposantApi
     * @apiGroup ComposantApi
     * @apiVersion 0.0.1
     * @api {post} /composant/:id Modifie un élément
     * @apiPermission {role} | Composant | update
     * @apiUse UpdateGenericApi
     * @apiUse DataComposantApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteComposantApi
     * @apiGroup ComposantApi
     * @apiVersion 0.0.1
     * @api {delete} /composant/:id Supprime un élément
     * @apiPermission {role} | Composant | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
