<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Provider;

use App\Library\Plugin\NotFoundPlugin;
use App\Library\Plugin\PreFlightPlugin;
use App\Library\Plugin\SecurityPlugin;
use App\Library\Router\RestGroup;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router;

/**
 * Class MvcProvider
 * @package App\Library\Provider
 */
class MvcProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     *
     * @param \Phalcon\DiInterface $di
     */
    public function register(\Phalcon\DiInterface $di)
    {
        /*
         |--------------------------------------------------------------------------
         | Service de routage
         |--------------------------------------------------------------------------
         | Le service de routage permet à l'application de décider en fonction
         | des routes définies dans celle-ci, où ira l'utilisateur par rapport
         | à l'url et à la méthode de requête.
         |
         */
        $di->setShared('router', function() use($di) {
            $eventsManager = $di->getShared('eventsManager');

            $router = new Router(false);
            $router->setDefaultNamespace('App\\Controller');
            $router->removeExtraSlashes(true);
            $router->setEventsManager($eventsManager);
            $router->mount(new RestGroup());
            return $router;
        });

        /*
         |--------------------------------------------------------------------------
         | Service de dispatching
         |--------------------------------------------------------------------------
         | Ce service permet à l'application de gérer des événements tel que
         | l'authentification, les exceptions et les routes inconnues, ce service
         | se comporte comme un middleware au yeux du routeur.
         |
         */
        $di->setShared('dispatcher', function() use($di) {
            $eventsManager = $di->getShared('eventsManager');
            $eventsManager->attach("dispatch:beforeDispatch", new PreFlightPlugin);
            $eventsManager->attach('dispatch:beforeExecuteRoute', new SecurityPlugin);
            $eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);

            $dispatcher = new Dispatcher;
            $dispatcher->setEventsManager($eventsManager);
            return $dispatcher;
        });
    }
}
