<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Task;

use App\Library\Helper;
use App\Library\Task\TaskBase;
use App\Model\Utilisateur;
use Phalcon\Filter;

/**
 * Class AdminTask
 * @package App\Task
 */
class AdminTask extends TaskBase
{
    /**
     * Default way
     */
    public function mainAction()
    {
        self::nl("Tâches d'administration de l'application,");
        self::nl("Voici la liste des commandes disponibles : ");

        self::listMethods($this);
    }

    public function generateAction()
    {
        $mdp = (new \Phalcon\Security())->hash('google88');

        $utilisateur = new Utilisateur;
        $utilisateur->setNom('Admin');
        $utilisateur->setPrenom('Admin');
        $utilisateur->setEmail('admin@madera.local');
        $utilisateur->setRole('Administrateur');

        $utilisateur->setMdp($mdp, $mdp);

        $utilisateur->create();
    }

    /**
     * Méthode de réinitialisation du mot de passe utilisateur
     *
     * @param array $params
     */
    public function resetpassAction($params = [])
    {
        self::nl("Tâche de mise à jour du mot de passe d'un utilisateur,");

        if (isset($params[0])) {

            /** @var Utilisateur $user */
            $user = Utilisateur::findFirst(
                [
                    'conditions' => 'email = ?1',
                    'bind'       => [
                        1 => $params[0] ?? null
                    ]
                ]
            );

            self::nl("Recherche de l'utilisateur en cours ...");

            if ($user !== false) {

                self::nl("L'utilisateur <" . $user->getNom() . ' ' . $user->getPrenom() . "> à été trouvé,");
                self::nl("Voulez-vous modifier sont mot de passe ? [o/N] ", null, null, false);

                if (strtolower(self::fgets()) == 'o') {

                    self::nl("Nouveau mot de passe : ", 'blue', null, false);

                    $mdp = $this->filter->sanitize(self::fgets(true), Filter::FILTER_TRIM);

                    self::nl();

                    self::nl("Répéter : ", 'blue', null, false);

                    $mdpConf = $this->filter->sanitize(self::fgets(true), Filter::FILTER_TRIM);

                    self::nl(PHP_EOL);

                    $user->setMdp($mdp, $mdpConf);

                    if (!$user->save()) {

                        self::nl('Erreur(s) : ', 'red');
                        self::nl(' -> ' . implode(PHP_EOL . ' -> ', Helper::getMessages($user)), 'red');

                        exit(255);
                    }

                    self::nl("Le mot de passe de l'utilisateur numéro <" . $user->getId() . "> à bien été changé.", 'green');
                }

                self::nl('Fin de la tâche.');

                exit(255);
            }

            self::nl("L'utilisateur recherché est introuvable !", 'red');
            exit(255);
        }

        self::nl("Vous devez spécifier l'adresse email de l'utilisateur en paramètre !" . PHP_EOL, 'red');
        self::nl('Exemple : ', 'yellow');
        self::nl(" -> ./console admin resetpass [adresse-email@domain.tld]", 'yellow');
    }
}
