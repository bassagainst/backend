<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Helper;
use App\Library\Model\ModelBase;
use App\Model\LiaisonModuleComposant;
use App\Model\Module;
use App\Model\Gamme;
use App\Model\NatureModule;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class ModuleController
 * @package App\Controller
 *
 * @apiDefine DataModuleApi
 * @apiParam {Number} id_gamme Identifiant de la gamme du module
 * @apiParam {Number} id_nature Identifiant de la nature du module
 * @apiParam {String} nom Nom du module
 * @apiParam {Float} marge Marge du module
 * @apiParam {String} description Description du module
 */
class ModuleController extends ControllerApi
{
    /** @var string $model */
    protected $model = Module::class;

    /** @var array $columns */
    protected $columns = ['id', 'id_gamme', 'id_nature', 'nom', 'marge', 'description'];

    /** @var array $join */
    protected $join = [
        [
            'class' => Gamme::class,
            'columns' => [
                'nom' => 'gamme_nom'
            ]
        ],
        [
            'class' => NatureModule::class,
            'columns' => [
                'nom' => 'naturemodule_nom'
            ]
        ]
    ];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function filters(Criteria $criteria)
    {
        $isModele = $this->request->getPost('isModele', Filter::FILTER_INT_CAST) ?? 1;
        $criteria->where('isModele = :isModele:');
        $criteria->bind(['isModele' => $isModele]);
    }

    /**
     * Méthode d'affichage des composants liés au module,
     * Méthode d'ajout d'une liaison entre un composant et un module,
     * Méthode de suppression d'une liaison entre le composant et un module
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function composantAction($id = null)
    {
        if ($this->request->isPost()
            && is_null($id)) {

            return $this->addLiaisonModuleComposant();

        } elseif ($this->request->isDelete()
            && isset($id)) {

            return $this->deleteLiaisonModuleComposant($id);

        } elseif ($this->request->isGet()
            && isset($id)) {

            return $this->manyToMany(LiaisonModuleComposant::class, $id,
                ['id_fournisseur', 'id_famille_composant', 'colisage', 'nom',
                    'prix_ht', 'marge', 'description', 'unite_colisage', 'id_tva'],
                ['id', 'quantite']
            );
        }

        return [];
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $idDevis = $this->request->getPost('id_devis', Filter::FILTER_INT_CAST);
        $criteria->andWhere('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
        if (isset($idDevis)) $criteria->andWhere('id_devis=:idDevis:', ['idDevis' => $idDevis]);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Module $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setIdGamme($this->request->getPost('id_gamme', Filter::FILTER_INT_CAST, $model->getIdGamme()));
        $model->setIdNature($this->request->getPost('id_nature', Filter::FILTER_INT_CAST, $model->getIdNature()));
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setMarge($this->request->getPost('marge', Filter::FILTER_FLOAT_CAST, $model->getMarge()));
        $model->setDescription($this->request->getPost('description', Filter::FILTER_STRIPTAGS, $model->getDescription()));
        $model->setIsModele(true);
    }

    /**
     * Méthode d'ajout d'une liaison entre un composant et un module
     *
     * @return array
     * @throws \Exception
     */
    private function addLiaisonModuleComposant()
    {
        $idModule = $this->request->getPost('id_module', Filter::FILTER_INT_CAST) ?? 0;
        $idComposant = $this->request->getPost('id_composant', Filter::FILTER_INT_CAST) ?? 0;
        $quantite = $this->request->getPost('quantite', Filter::FILTER_INT_CAST) ?? 0;

        $liaison = new LiaisonModuleComposant;
        $liaison->setIdModule($idModule);
        $liaison->setIdComposant($idComposant);
        $liaison->setQuantite($quantite);

        if ($liaison->create() !== false) {

            $this->response->setStatusCode(201, 'Created');

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $liaison->toArray()
            ];
        }

        $this->response->setStatusCode(409, 'Conflict');

        return [
            'status' => 'FAILED',
            'messages' => Helper::getMessages($liaison)
        ];
    }

    /**
     * Méthode de suppression d'une liaison entre le composant et un module
     *
     * @param $liaisonId
     * @return array
     * @throws \Exception
     */
    private function deleteLiaisonModuleComposant($liaisonId)
    {
        $liaisonIdFiltered = $this->filter->sanitize($liaisonId, Filter::FILTER_ABSINT);

        /** @var LiaisonModuleComposant $liaison */
        $liaison = LiaisonModuleComposant::findFirst($liaisonIdFiltered);

        if ($liaison !== false) {

            if ($liaison->delete()) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'message' => "La ressource numéro " . $liaison->getId() . " à été supprimée !"
                ];
            }

            $this->response->setStatusCode(409, 'Conflict');

            return [
                'status' => 'FAILED',
                'messages' => Helper::getMessages($liaison)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchModuleApi
     * @apiGroup ModuleApi
     * @apiVersion 0.0.1
     * @api {post} /module/search Recherche des éléments
     * @apiPermission {role} | Module | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateModuleApi
     * @apiGroup ModuleApi
     * @apiVersion 0.0.1
     * @api {post} /module Ajoute un nouvel élément
     * @apiPermission {role} | Module | create
     * @apiUse CreateGenericApi
     * @apiUse DataModuleApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadModuleApi
     * @apiGroup ModuleApi
     * @apiVersion 0.0.1
     * @api {get} /module/:id Affiche un élément
     * @apiPermission {role} | Module | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateModuleApi
     * @apiGroup ModuleApi
     * @apiVersion 0.0.1
     * @api {post} /module/:id Modifie un élément
     * @apiPermission {role} | Module | update
     * @apiUse UpdateGenericApi
     * @apiUse DataModuleApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteModuleApi
     * @apiGroup ModuleApi
     * @apiVersion 0.0.1
     * @api {delete} /module/:id Supprime un élément
     * @apiPermission {role} | Module | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
