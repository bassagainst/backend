<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Builder\Project\Simple;
use Phalcon\Security;

/**
 * Class Projet
 * @package App\Model
 *
 * @property Simple|Client[]           $client
 * @property Simple|Utilisateur[]      $utilisateur
 *
 * @method   Simple|Gamme[]            getClient($parameters = null)
 * @method   Simple|Utilisateur[]      getUtilisateur($parameters = null)
 *
 * @method   integer                   countClient()
 * @method   integer                   countUtilisateur()
 */
class Projet extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $reference */
    protected $reference;

    /** @var string $date_creation */
    protected $date_creation;

    /** @var int $id_client */
    protected $id_client;

    /** @var int $id_utilisateur */
    protected $id_utilisateur;

    /** @var string $nom */
    protected $nom;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Projet");

        $this->hasMany('id', 'App\Model\Devis', 'id_projet',
            [
                'alias' => 'Devis',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('id_client', 'App\Model\Client', 'id',
            [
                'alias' => 'Client',
                'foreignKey' => [
                    'message' => "Le client choisi pour ce projet n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_utilisateur', 'App\Model\Utilisateur', 'id',
            [
                'alias' => 'Utilisateur',
                'foreignKey' => [
                    'message' => "L'utilisateur choisi pour ce projet n'existe pas"
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Projet';
    }

    /**
     * @inheritdoc
     *
     * @throws Security\Exception
     */
    public function beforeValidationOnCreate()
    {
        $security = new Security\Random;

        $this->date_creation = (new \DateTime)->format('Y-m-d H:i:s');

        $this->reference = "MDRP" . sprintf('%04d', time() . $security->number(16));
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return is_string($this->date_creation)
            ? (new \DateTime($this->date_creation))->format('d/m/Y') ?? null
            : null;
    }

    /**
     * Getter pour la référence
     *
     * @return string
     */
    public function getReference()
    {
        return "MDRP" . sprintf('%05d', $this->getId());
    }

    /**
     * Getter pour l'id client
     *
     * @return int
     */
    public function getIdClient()
    {
        return (int) $this->id_client;
    }

    /**
     * Setter pour l'id client
     *
     * @param int $id_client
     */
    public function setIdClient(int $id_client)
    {
        $this->id_client = $id_client;
    }

    /**
     * Getter pour l'id utilisateur
     *
     * @return int
     */
    public function getIdUtilisateur()
    {
        return (int) $this->id_utilisateur;
    }

    /**
     * Setter pour l'id utilisateur
     *
     * @param int $id_utilisateur
     */
    public function setIdUtilisateur(int $id_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

}
