<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Provider;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Events\Manager as EventsManager;

/**
 * Class FactoryDefaultProvider
 * @package App\Library\Provider
 */
class FactoryDefaultProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     *
     * @param \Phalcon\DiInterface $di
     */
    public function register(\Phalcon\DiInterface $di)
    {
        /*
         |--------------------------------------------------------------------------
         | Service de gestion des événements
         |--------------------------------------------------------------------------
         | Ce service permet la gestion des événements au sein de l'application,
         | il se comporte comme un middleware, il peu donc agir avant, pendant ou
         | après une action au sein de l'application, par exemple <beforeDelete>.
         |
         */
        $di->setShared('eventsManager', new EventsManager);

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des requêtes
         |--------------------------------------------------------------------------
         | Ce service permet la gestion des requêtes au sein de l'application.
         |
         */
        $di->setShared('request', new Request);

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des réponses
         |--------------------------------------------------------------------------
         | Ce service permet la gestion des réponses au sein de l'application.
         |
         */
        $di->setShared('response', new Response);

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des filtres
         |--------------------------------------------------------------------------
         | Ce service permet la gestion des filtres au sein de l'application.
         |
         */
        $di->setShared('filter', new Filter);
    }
}