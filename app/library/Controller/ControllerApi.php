<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Controller;

use App\Library\Helper;
use App\Library\Model\ModelBase;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * Class ControllerApi
 * @package App\Library\Controller
 */
abstract class ControllerApi extends ControllerSecurity implements ControllerApiInterface
{
    /** @var ModelBase $model */
    protected $model;

    /** @var int $offset */
    protected $offset;

    /** @var int $limit */
    protected $limit = 8;

    /** @var array $columns */
    protected $columns = [];

    /** @var array $join */
    protected $join = [];

    /**
     * Méthode d'affichage des élements ManyToMany
     *
     * @param $liaison
     * @param null $originId
     * @param array $cols
     * @param array $extra
     * @return array
     * @throws \Exception
     */
    protected function manyToMany($liaison, $originId = null, $cols = [], $extra = [])
    {
        $result = [];

        /** @var ModelBase $model */
        $model = $this->model::findFirst($originId);

        if ($model !== false) {

            $reflexion = new \ReflectionClass($liaison);

            /** @var ModelBase $row */
            foreach ($model->getRelated($reflexion->getShortName()) as $k => $row) {

                /** @var ModelBase $extraArray */
                $extraArray = $model->getRelated($reflexion->getShortName(),
                    [
                        'columns' => $liaison . '.*'
                    ]
                )[$k];

                $result[] = array_merge(
                    $row->toArray($cols),
                    $extraArray->toArray($extra)
                );
            }

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status'  => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data'    => $result,
                'total'   => count($result)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Middleware de gestion des filtres de l'api
     *
     * @param Criteria $criteria
     * @return void
     */
    protected function filters(Criteria $criteria)
    {
        // Par défaut ne fait rien
    }

    /**
     * Middleware de gestion des paramètres de recherches d'une ressource
     *
     * @param Criteria $criteria
     * @return void
     */
    protected function atSearch(Criteria $criteria)
    {
        // Par défaut ne fait rien
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \Exception
     *
     * @apiDefine SearchGenericApi
     * @apiName SearchGenericApi
     * @apiGroup GenericApi
     * @apiVersion 0.0.1
     * @api {post} /:controller/search Recherche des éléments
     * @apiParam {Number} limit Limite le nombre de résultats
     * @apiParam {Number} offset Ignore un nombre de résultats
     * @apiParam {String} order Arrange l'ordre des résultats
     * @apiParam {String} query Filtre les résultats
     *
     * @apiSuccess {Object} data Affiche les données de l'objet
     * @apiSuccess {Object} acl Affiche les droits de l'utilisateur
     * @apiSuccess {Number} total Affiche le nombre total d'enregistrements
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Number} latency Affiche la vitesse de réponse de l'API
     * @apiError Unauthorized Vous devez être connecté pour accéder à cette ressource
     * @apiError Forbidden Vous n'avez pas les droits pour accéder à cette ressource
     */
    public function searchAction(): array
    {
        $limit = $this->request->getPost('limit', Filter::FILTER_INT_CAST);
        $offset = $this->request->getPost('offset', Filter::FILTER_INT_CAST);
        $orderBy = $this->request->getPost('order', Filter::FILTER_STRIPTAGS);

        $criteria = $this->model::query();

        $this->filters($criteria);

        if ($this->request->isPost()) $this->atSearch($criteria);

        /** @var ModelBase $instance */
        $instance = (new $this->model);

        if ($instance->getModelsMetaData()->hasAttribute($instance, 'deleted')) {

            if (is_null($criteria->getWhere())) {

                $criteria->where('deleted = 0');

            } else {

                $criteria->andWhere('deleted = 0');
            }
        }

        $criteriaCount = clone $criteria;

        $criteriaCount->columns([$this->model . '.id']);

        if ($this->request->isPost()) {

            $criteria->limit($limit ?? $this->limit, $offset ?? $this->offset);

        } else {

            $criteria->limit(1000, 0);
        }

        if (!empty($orderBy)) $criteria->orderBy($orderBy);

        $models = $criteria->execute();

        if ($models !== false) {

            $result = [];

            /** @var ModelBase $model */
            foreach ($models as $model) {

                $result[] = $this->fetchSearch($model);
            }

            $counter = $criteriaCount->execute();

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status'  => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data'    => $result,
                'acl'     => Helper::searchAcls($this->model),
                'total'   => count($counter->toArray())
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Middleware de gestion des informations retournés lors de la recherche d'une ressource
     *
     * @param Simple|ModelBase $model
     * @return array
     * @throws \Exception
     */
    protected function fetchSearch($model): array
    {
        $fetch = [];

        $fetch += $model->toArray(!empty($this->columns) ? $this->columns : null);

        if (isset($this->join)) {

            foreach ($this->join as $join) {

                $cols = [];

                foreach ($join['columns'] as $label => $column) {

                    $cols[] = $label;
                }

                $class = 'get' . (new \ReflectionClass($join['class']))->getShortName();

                $toArray = $model->$class()->toArray($cols);

                foreach ($join['columns'] as $label => $column) {
                    foreach ($toArray as $k => $tA) {

                        if ($k === $label) $fetch[$column] = $tA;
                    }
                }
            }
        }

        return $fetch;
    }

    /**
     * Middleware de gestion des information pour la création et la modification d'une ressource
     *
     * Doit être utilisé pour nourrir le modèle de données
     *
     * @param ModelBase $model
     * @throws \Exception
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        unset($model);

        $this->response->setStatusCode(501, 'Not Implemented');

        throw new \Exception($this->unmappedMessage('beforeCreateUpdate'));
    }

    /**
     * Middleware de gestion des informations pour la création d'une ressource
     *
     * Doit être utilisé pour nourrir le modèle de données
     *
     * @param ModelBase $model
     * @throws \Exception
     */
    protected function beforeCreate(ModelBase $model)
    {
        unset($model);

        throw new \Exception($this->unmappedMessage('beforeCreate'));
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \Exception
     *
     * @apiDefine CreateGenericApi
     * @apiName CreateGenericApi
     * @apiGroup GenericApi
     * @apiVersion 0.0.1
     * @api {post} /:controller Ajoute un nouvel élément
     *
     * @apiSuccess {Object} data Affiche les données de l'objet
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Number} latency Affiche la vitesse de réponse de l'API
     * @apiError Unauthorized Vous devez être connecté pour accéder à cette ressource
     * @apiError Forbidden Vous n'avez pas les droits pour accéder à cette ressource
     * @apiError Conflict Vous devez envoyer les bon paramètres à l'application
     */
    public function createAction(): array
    {
        /** @var ModelBase $model */
        $model = new $this->model;

        try {

            $this->beforeCreate($model);

        } catch (\Exception $e) {

            $this->beforeCreateUpdate($model);
        }

        if ($model->create() !== false) {

            $this->response->setStatusCode(201, 'Created');

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status'  => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data'    => $this->afterCreate($model)
            ];
        }

        $this->response->setStatusCode(409, 'Conflict');

        return [
            'status'   => 'FAILED',
            'messages' => Helper::getMessages($model)
        ];
    }

    /**
     * Middleware de gestion des informations retournés après la création d'une ressource
     *
     * @param ModelBase $model
     * @return array
     * @throws \Exception
     */
    protected function afterCreate(ModelBase $model): array
    {
        return $model->toArray(!empty($this->columns) ? $this->columns : null);
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \ReflectionException
     *
     * @apiDefine ReadGenericApi
     * @apiName ReadGenericApi
     * @apiGroup GenericApi
     * @apiVersion 0.0.1
     * @api {get} /:controller/:id Affiche un élément
     *
     * @apiSuccess {Object} data Affiche les données de l'objet
     * @apiSuccess {Object} acl Affiche les droits de l'utilisateur
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Number} latency Affiche la vitesse de réponse de l'API
     * @apiError Unauthorized Vous devez être connecté pour accéder à cette ressource
     * @apiError Forbidden Vous n'avez pas les droits pour accéder à cette ressource
     * @apiError Conflict Vous devez envoyer les bon paramètres à l'application
     */
    public function readAction(): array
    {
        $id = $this->dispatcher->getParam('id', Filter::FILTER_ABSINT);

        $criteria = $this->model::query();

        $reflection = new \ReflectionClass($this->model);

        $this->filters($criteria);

        $criteria->andWhere($reflection->getName() . '.id = :id:', ['id' => $id]);

        $criteria->limit(1);

        $model = $criteria->execute()->getFirst();

        if ($model !== false) {

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'acl'     => Helper::searchAcls($this->model),
                'data' => $this->afterRead($model)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Middleware de gestion des informations envoyés après la lecture d'une ressource
     *
     * @param mixed $model
     * @return array
     * @throws \ReflectionException
     */
    protected function afterRead($model): array
    {
        $read = [];

        $read += $model->toArray(!empty($this->columns) ? $this->columns : null);

        if (isset($this->join)) {

            foreach ($this->join as $join) {

                $cols = [];

                foreach ($join['columns'] as $label => $column) {

                    $cols[] = $label;
                }

                $class = 'get' . (new \ReflectionClass($join['class']))->getShortName();

                $toArray = $model->$class()->toArray($cols);

                foreach ($join['columns'] as $label => $column) {
                    foreach ($toArray as $k => $tA) {

                        if ($k === $label) $read[$column] = $tA;
                    }
                }
            }
        }

        return $read;
    }

    /**
     * Middleware de gestion des informations
     *
     * @param ModelBase $model
     * @throws \Exception
     */
    protected function beforeUpdate(ModelBase $model)
    {
        unset($model);

        throw new \Exception($this->unmappedMessage('beforeUpdate'));
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \Exception
     *
     * @apiDefine UpdateGenericApi
     * @apiName UpdateGenericApi
     * @apiGroup GenericApi
     * @apiVersion 0.0.1
     * @api {post} /:controller/:id Modifie un élément
     *
     * @apiSuccess {Object} data Affiche les données de l'objet
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Number} latency Affiche la vitesse de réponse de l'API
     * @apiError Unauthorized Vous devez être connecté pour accéder à cette ressource
     * @apiError Forbidden Vous n'avez pas les droits pour accéder à cette ressource
     * @apiError Conflict Vous devez envoyer les bon paramètres à l'application
     */
    public function updateAction(): array
    {
        $id = $this->dispatcher->getParam('id', Filter::FILTER_ABSINT);

        /** @var ModelBase $model */
        $model = $this->model::findFirst($id);

        if ($model !== false) {

            try {

                $this->beforeUpdate($model);

            } catch (\Exception $e) {

                $this->beforeCreateUpdate($model);
            }

            if ($model->update() !== false) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'data' => $this->afterUpdate($model)
                ];
            }

            $this->response->setStatusCode(409, 'Conflict');

            return [
                'status' => 'FAILED',
                'messages' => Helper::getMessages($model)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Middleware de gestion des informations retournés lors de la mise à jour d'une ressource
     *
     * @param ModelBase $model
     * @return array
     * @throws \Exception
     */
    protected function afterUpdate(ModelBase $model): array
    {
        return $model->toArray(!empty($this->columns) ? $this->columns : null);
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \Exception
     *
     * @apiDefine DeleteGenericApi
     * @apiName DeleteGenericApi
     * @apiGroup GenericApi
     * @apiVersion 0.0.1
     * @api {delete} /:controller/:id Supprime un élément
     *
     * @apiSuccess {String} message Message de résultat d'une supression
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Number} latency Affiche la vitesse de réponse de l'API
     * @apiError Unauthorized Vous devez être connecté pour accéder à cette ressource
     * @apiError Forbidden Vous n'avez pas les droits pour accéder à cette ressource
     */
    public function deleteAction(): array
    {
        $id = $this->dispatcher->getParam('id', Filter::FILTER_ABSINT);

        /** @var ModelBase $model */
        $model = $this->model::findFirst($id);

        if ($model !== false) {

            if ($model->delete()) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'message' => $this->afterDelete($model)
                ];
            }

            $this->response->setStatusCode(409, 'Conflict');

            return [
                'status' => 'FAILED',
                'messages' => Helper::getMessages($model)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Middleware de gestion du message de suppression d'une ressource
     *
     * @param ModelBase $model
     * @return string
     */
    protected function afterDelete(ModelBase $model): string
    {
        return "La ressource numéro " . $model->getId() . " à été supprimée !";
    }

    /**
     * Méthode d'affichage du message générique de ressource non trouvée
     *
     * @return string
     */
    protected function notFoundMessage(): string
    {
        $this->response->setStatusCode(404, 'Not Found');

        return "La ressource demandée n'existe pas !";
    }

    /**
     * Méthode de customisation des messages d'exceptions (code 501)
     *
     * @param string $method
     * @return string
     */
    protected function unmappedMessage(string $method): string
    {
        return "Le serveur n'à pas pu définir correctement <$method>, veuillez contacter votre administrateur système.";
    }
}
