<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Plugin;

use App\Library\Helper;
use App\Library\Controller\ControllerSecurity;
use App\Model\Utilisateur;
use Phalcon\Events\Event;
use Phalcon\Filter;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;

class SecurityPlugin extends Plugin
{
    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws \ReflectionException
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        // Vérifie si la ressource utilise le controlleur d'authentification
        // Si ce n'est pas le cas, autorise l'application à continuer l'exécution
        if ($dispatcher->getActiveController() instanceof ControllerSecurity) {

            return $this->securize($dispatcher);
        }

        return true;
    }

    /**
     * Déclanche la méthode de sécurité
     *
     * @param $dispatcher
     * @return bool
     * @throws \ReflectionException
     */
    public function securize($dispatcher)
    {
        if (!empty($this->request->getHeader('Authorization'))) {

            if ($this->authenticate()) {

                if ($this->authorize($dispatcher)) {

                    return true;
                }

                $this->response->setStatusCode(403, 'Forbidden');

                $this->response->setJsonContent(
                    [
                        'status' => 'FAILED',
                        'message' => "Vous n'avez pas les droits pour accéder à cette page !"
                    ]
                );

            } else {

                $this->response->setStatusCode(401, 'Unauthorized');

                $this->response->setJsonContent(
                    [
                        'status' => 'FAILED',
                        'message' => "L'authentification à échouée ou expirée !"
                    ]
                );
            }

        } else {

            $this->response->setStatusCode(401, 'Unauthorized');

            $this->response->setJsonContent(
                [
                    'status' => 'FAILED',
                    'message' => 'Vous êtes pas connecté !'
                ]
            );
        }

        return false;
    }

    /**
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws \ReflectionException
     */
    public function authorize($dispatcher)
    {
        /** @var \Phalcon\Acl\AdapterInterface $acl */
        $acl = $this->getDI()->getShared('acl');

        $session = Helper::getTokenSession();

        $reflection = new \ReflectionClass($dispatcher->getControllerClass());

        $role = $session->role;
        $resource = strstr($reflection->getShortName(), 'Controller', true);
        $access = $dispatcher->getActionName();

        return $acl->isAllowed($role, $resource, $access);
    }

    public function authenticate() :bool
    {
        $authorizationHeader = $this->request->getHeader('Authorization');

        switch ($this->getAuthorizationType()) {
            case 'Basic':

                $filter = new Filter;
                $credentials = $this->request->getBasicAuth();

                $email = $filter->sanitize($credentials['username'], Filter::FILTER_EMAIL);
                $mdp = $filter->sanitize($credentials['password'], Filter::FILTER_STRIPTAGS);

                $utilisateur = Utilisateur::findFirst(
                    [
                        'conditions' => 'email = ?1',
                        'bind'       => [
                            1 => $email
                        ]
                    ]
                );

                if ($utilisateur !== false
                    && $utilisateur instanceof Utilisateur
                    && $utilisateur->checkMdp($mdp)) {

                    return true;
                }

                break;

            case 'Bearer':

                if (preg_match('/Bearer\s+(.*)$/i', $authorizationHeader, $matches)) {

                    try {

                        $data = Helper::decodeJwt($matches[1]);

                        return isset($data->payload);

                    } catch (\Exception $e) {

                        return false;
                    }
                }

                break;

            default:

                if ($this->request->hasQuery('authorization')) {

                    $authorization = $this->request->getQuery('authorization', Filter::FILTER_TRIM);

                    /** @var \Redis $redis */
                    $redis = $this->getDI()->get('redis');

                    $tokenPayload = $redis->get($authorization);

                    if ($tokenPayload !== false) {

                        return true;
                    }
                }

                break;
        }

        return false;
    }

    private function getAuthorizationType()
    {
        $authorizationHeader = $this->request->getHeader('Authorization');

        return !empty($authorizationHeader)
            ? substr($authorizationHeader, 0, strpos($authorizationHeader, ' '))
            : null;
    }
}
