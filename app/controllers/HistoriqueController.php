<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Controller\ControllerBase;
use App\Library\Helper;
use App\Library\Model\ModelBase;
use App\Library\Plugin\SecurityPlugin;
use App\Model\Historique;
use App\Model\Utilisateur;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * Class HistoriqueController
 * @package App\Controller
 */
class HistoriqueController extends ControllerBase
{
    /** @var ModelBase|Historique|string $model */
    protected $model = Historique::class;

    /** @var int $offset */
    protected $offset;

    /** @var int $limit */
    protected $limit = 8;

    /** @var array $columns */
    protected $columns = ['id', 'crud', 'date', 'table', 'id_table', 'id_utilisateur'];

    /** @var array $join */
    protected $join = [];

    public function beforeExecuteRoute()
    {
        $security = new SecurityPlugin;

        if ($security->authenticate()) {

            return true;

        } else {

            $this->response->setStatusCode(401, 'Unauthorized');

            $this->response->setJsonContent(
                [
                    'status' => 'FAILED',
                    'message' => "L'authentification à échouée ou expirée !"
                ]
            );

            return false;
        }
    }

    public function filters(Criteria $criteria)
    {
        $id_utilisateur = Helper::getTokenSession()->id_utilisateur;
        $role_utilisateur = Helper::getTokenSession()->role;

        if ($role_utilisateur !== 'Administrateur') {

            $criteria->andWhere('id_utilisateur = :id_utilisateur:', ['id_utilisateur' => $id_utilisateur]);
        }

        $criteria->orderBy('date DESC');
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $crud = $this->request->getPost('crud', Filter::FILTER_STRIPTAGS);
        $idUtilisateur = $this->request->getPost('id_utilisateur', Filter::FILTER_STRIPTAGS);
        $table = $this->request->getPost('table', Filter::FILTER_STRIPTAGS);

        if (isset($crud)) $criteria->andWhere('crud LIKE :crud:', ['crud' => $crud]);
        if (isset($idUtilisateur)) $criteria->andWhere('id_utilisateur LIKE :id_utilisateur:', ['id_utilisateur' => $idUtilisateur]);
        if (isset($table)) $criteria->andWhere('table LIKE :table:', ['table' => $table]);
    }

    /**
     * @param ModelBase|Simple|Historique $model
     * @return array
     * @throws \Exception
     */
    protected function fetchSearch($model): array
    {
        $fetch = [];

        $fetch += $model->toArray(!empty($this->columns) ? $this->columns : null);

        if (isset($this->join)) {

            foreach ($this->join as $join) {

                $cols = [];

                foreach ($join['columns'] as $label => $column) {

                    $cols[] = $label;
                }

                $class = 'get' . (new \ReflectionClass($join['class']))->getShortName();

                $toArray = $model->$class()->toArray($cols);

                foreach ($join['columns'] as $label => $column) {
                    foreach ($toArray as $k => $tA) {

                        if ($k === $label) $fetch[$column] = $tA;
                    }
                }
            }
        }

        switch ($fetch['crud']) {
            case 'C':
                $fetch['crud'] = "Création";
                break;

            case 'U':
                $fetch['crud'] = "Modification";
                break;

            case 'D':
                $fetch['crud'] = "Suppression";
                break;

            default:
                break;
        }

        if ($model->getIdUtilisateur() !== 0) {

            /** @var Utilisateur $utilisateur */
            $utilisateur = Utilisateur::findFirst($model->getIdUtilisateur());

            /** @var ModelBase $class */
            $class = 'App\\Model\\' . $model->getTable();

            $fetch['record'] = null;

            if (class_exists($class)) {

                $cible = $class::findFirst(
                    [
                        'conditions' => 'id = ?1',
                        'bind' => [
                            1 => $model->getIdTable()
                        ]
                    ]
                );

                if ($cible !== false) {

                    $fetch['record'] = $cible->toArray();
                }
            }

            if ($utilisateur !== false) {

                $fetch['utilisateur_nom'] = $utilisateur->getNom() . ' ' . $utilisateur->getPrenom();

                return $fetch;
            }

            $fetch['utilisateur_nom'] = 'Utilisateur Inconnu';

            return $fetch;
        }

        $fetch['utilisateur_nom'] = 'Application Cli';

        return $fetch;
    }

    /**
     * @inheritdoc
     *
     * @return array
     * @throws \Exception
     *
     * @apiName SearchHistoriqueApi
     * @apiGroup HistoriqueApi
     * @apiVersion 0.0.1
     * @api {post} /historique/search Recherche des éléments
     * @apiPermission {role} | Historique | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        $limit = $this->request->getPost('limit', Filter::FILTER_INT_CAST);
        $offset = $this->request->getPost('offset', Filter::FILTER_INT_CAST);
        $orderBy = $this->request->getPost('order', Filter::FILTER_STRIPTAGS);

        $criteria = $this->model::query();

        $this->filters($criteria);

        if ($this->request->isPost()) $this->atSearch($criteria);

        /** @var ModelBase $instance */
        $instance = (new $this->model);

        if ($instance->getModelsMetaData()->hasAttribute($instance, 'deleted')) {

            if (is_null($criteria->getWhere())) {

                $criteria->where('deleted = 0');

            } else {

                $criteria->andWhere('deleted = 0');
            }
        }

        $criteriaCount = clone $criteria;

        $criteriaCount->columns([$this->model . '.id']);

        $criteria->limit($limit ?? $this->limit, $offset ?? $this->offset);

        if (!empty($orderBy)) $criteria->orderBy($orderBy);

        $models = $criteria->execute();

        if ($models !== false) {

            $result = [];

            /** @var ModelBase $model */
            foreach ($models as $model) {

                $result[] = $this->fetchSearch($model);
            }

            $counter = $criteriaCount->execute();

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status'  => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data'    => $result,
                'acl'     => Helper::searchAcls($this->model),
                'total'   => count($counter->toArray())
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }
}
