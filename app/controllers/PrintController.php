<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerBase;
use App\Library\Controller\ControllerSecurity;
use App\Library\Plugin\SecurityPlugin;
use App\Model\Composant;
use App\Model\Devis;
use App\Model\LiaisonModuleComposant;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;

/**
 * Class PrintDevisController
 * @package App\Controller
 */
class PrintController extends ControllerBase
{
    /**
     * Retourne le PDF d'un devis
     *
     * @param null $devisId
     * @throws \Exception
     */
    public function devisAction($devisId = null)
    {
        $security = new SecurityPlugin;

        if ($security->authenticate()) {

            /** @var Devis $devis */
            $devis = Devis::findFirst($devisId);

            if ($devis !== false) {

                $size = 0;
                $tvaList = [];
                $products = [];
                $totalHt = 0;
                $totalTtc = 0;

                foreach ($devis->getModule() as $module) {

                    if ($module->isDeleted()) continue;

                    $size++;
                    $products[$module->getNom()] = [];

                    /** @var Composant $composant */
                    foreach ($module->getLiaisonModuleComposant() as $k => $composant) {

                        if ($composant->isDeleted()) continue;

                        /** @var LiaisonModuleComposant $extraObject */
                        $extraObject = $module->getRelated('LiaisonModuleComposant', [
                            'columns' => LiaisonModuleComposant::class . '.*'
                        ])[$k];

                        $extraArray = $extraObject->toArray(['id', 'quantite']);
                        $tvaArray = $composant->getTva()->toArray(['taux']);
                        $composantArray = $composant->toArray(['id_fournisseur', 'id_famille_composant', 'colisage', 'nom',
                            'prix_ht', 'marge', 'description', 'unite_colisage', 'id_tva']);

                        $size++;
                        $products[$module->getNom()][] = array_merge(
                            $composantArray,
                            $extraArray,
                            $tvaArray
                        );

                        $priceHt = $composantArray['prix_ht'] * $extraArray['quantite'];

                        if (empty($tvaList[(string)$tvaArray['taux']])) $tvaList[(string)$tvaArray['taux']] = 0;
                        $tvaList[(string)$tvaArray['taux']] += ($priceHt * $tvaArray['taux']) / 100;

                        $totalHt += $priceHt;
                        $totalTtc += $priceHt + (($priceHt * $tvaArray['taux']) / 100);
                    }
                }

                /** @var \Phalcon\Config $config */
                $config = $this->getDI()->getShared('config')->get('application');

                /** @var \Phalcon\Config $company */
                $company = $this->getDI()->getShared('config')->get('company');

                $view = new View\Simple;
                $view->setViewsDir($config->get('templatesDir'));
                $view->setDI($this->getDI());

                $render = $view->render('print/devis.phtml', [
                    'devis' => isset($devis) ? $devis->toArray() : [],
                    'company' => isset($company) ? $company->toArray() : [],
                    'products' => $products,
                    'tvaList' => $tvaList,
                    'totalHt' => number_format((float)$totalHt, 2, '.', ''),
                    'totalTtc' => number_format((float)$totalTtc, 2, '.', ''),
                    'size' => $size,
                    'maxsize' => 38
                ]);

                // Max size 38

                $this->response->setContent($render);
            }
        }
    }

    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Désactivation de l'affichage en Json
    }
}

