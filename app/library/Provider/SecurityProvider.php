<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Provider;

use Phalcon\Acl;
use Phalcon\Acl\Adapter\Database as AclAdapter;
use Phalcon\Acl\RoleAware;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Security;

/**
 * Class SecurityProvider
 * @package App\Library\Provider
 */
class SecurityProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     *
     * @param \Phalcon\DiInterface $di
     */
    public function register(\Phalcon\DiInterface $di)
    {
        /*
         |--------------------------------------------------------------------------
         | Service de sécurité
         |--------------------------------------------------------------------------
         | Ce service permet de gérer la sécurité de l'application, c'est grâce à
         | lui que nous hasherons les mot de passes par exemple, il permet aussi
         | d'utiliser l'algorithme Blowfish et d'assurer une forte sécurité au sein
         | du hashage grâce à un facteur de vitesse.
         |
         */
        $di->setShared('security', function () use ($di) {
            $config = $di->getShared('config')->get('security');

            $security = new Security;
            $security->setWorkFactor($config->workFactor ?? 10);
            return $security;
        });

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des droits ACL
         |--------------------------------------------------------------------------
         | Ce service permet de gérer les droits des utilisateurs au sein de
         | l'application ainsi que les groupes, héritages, etc.
         |
         */
        $di->setShared('acl', function () use ($di) {
            $acl = new AclAdapter(
                [
                    'db' => $di->getShared('db'),
                    'roles' => 'Roles',
                    'rolesInherits' => 'Roles_Inherits',
                    'resources' => 'Resources',
                    'resourcesAccesses' => 'Resources_Accesses',
                    'accessList' => 'Access_List'
                ]
            );
            $acl->setDefaultAction(Acl::DENY);
            $acl->setNoArgumentsDefaultAction(Acl::DENY);
            return $acl;
        });
    }
}
