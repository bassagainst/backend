<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * Class Historique
 * @package App\Model
 *
 * @property Simple|Utilisateur[] $utilisateur
 * @method   Simple|Utilisateur[] getUtilisateur($parameters = null)
 * @method   integer              countUtilisateur()
 */
class Historique extends ModelBase
{
    /** @var int TTL_CACHE */
    const TTL_CACHE = -1;

    /** @var string $date */
    protected $date;

    /** @var string $crud */
    protected $crud;

    /** @var integer $id_utilisateur */
    protected $id_utilisateur;

    /** @var string $table */
    protected $table;

    /** @var int $id_table */
    protected $id_table;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Historique");

        $this->belongsTo('id_utilisateur', 'App\Model\Utilisateur', 'id',
            [
                'alias' => 'Utilisateur'
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Historique';
    }

    /**
     * @inheritdoc
     */
    public function beforeValidationOnCreate()
    {
        $this->date = (new \DateTime)->format('Y-m-d H:i:s');
    }

    /**
     * Getter pour la date
     *
     * @return string
     */
    public function getDate()
    {
        return is_string($this->date)
            ? (new \DateTime($this->date))->format('d/m/Y H:i:s') ?? null
            : null;
    }

    /**
     * Getter pour le crud
     *
     * @return string
     */
    public function getCrud()
    {
        return $this->crud;
    }

    /**
     * Setter pour le crud
     *
     * @param string $crud
     */
    public function setCrud(string $crud)
    {
        $this->crud = $crud;
    }

    /**
     * Getter pour l'id utilisateur
     *
     * @return int
     */
    public function getIdUtilisateur()
    {
        return (int)$this->id_utilisateur;
    }

    /**
     * Setter pour l'id utilisateur
     *
     * @param int $id_utilisateur
     */
    public function setIdUtilisateur(int $id_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
    }

    /**
     * Getter pour la table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Setter pour la table
     *
     * @param string $table
     */
    public function setTable(string $table)
    {
        $this->table = $table;
    }

    /**
     * Getter pour l'id de la table
     *
     * @return int
     */
    public function getIdTable()
    {
        return (int)$this->id_table;
    }

    /**
     * Setter pour l'id de la table
     *
     * @param int $id_table
     */
    public function setIdTable(int $id_table)
    {
        $this->id_table = $id_table;
    }
}
