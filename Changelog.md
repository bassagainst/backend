# Migrations
## 1.0.0
Mise en place de la structure de données générale, concerne les tables suivantes :
```bash
Client               Facture                   Mode_Reglement  Type_Finition
Composant            Famille_Composant         Module          Type_Gamme
Condition_Reglement  Fournisseur               Nature_Module   Type_Isolant
Devis                Gamme                     Projet          Utilisateur
Devis_Facture        Historique                Tva
Etat_Devis           Liaison_Module_Composant  Type_Facture
```
## 1.0.1
Modification de la structure, oubli des AI, ajout des AI, concerne les tables suivantes :
```bash
Client               Facture                   Mode_Reglement  Type_Finition
Composant            Famille_Composant         Module          Type_Gamme
Condition_Reglement  Fournisseur               Nature_Module   Type_Isolant
Devis                Gamme                     Projet          Utilisateur
Devis_Facture        Historique                Tva
Etat_Devis           Liaison_Module_Composant  Type_Facture
```
