<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Helper;
use App\Library\Model\ModelBase;
use App\Model\Devis;
use App\Model\Module;
use App\Model\Projet;
use App\Model\EtatDevis;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class DevisController
 * @package App\Controller
 *
 * @apiDefine DataDevisApi
 * @apiParam {Number} id_projet Identifiant du projet du devis
 * @apiParam {Number} id_etat Identifiant de l'état du devis
 * @apiParam {Date} date_fin_validite Date de la fin de validité du devis
 * @apiParam {String} note_publique Note publique du devis
 * @apiParam {String} note_privee Note privée du devis
 * @apiParam {Float} remise Remise du devis
 */
class DevisController extends ControllerApi
{
    /** @var string $model */
    protected $model = Devis::class;

    /** @var array $columns */
    protected $columns = ['id', 'id_projet', 'id_etat', 'reference', 'date',
        'date_fin_validite', 'note_publique', 'note_privee', 'remise', 'nom_client', 'prenom_client',
        'numero_rue_client', 'rue_client', 'ville_client', 'cp_client', 'email_client', 'tel_client'];

    /** @var array $join */
    protected $join = [
        [
            'class' => Projet::class,
            'columns' => [
                'label' => 'projet_nom'
            ]
        ],
        [
            'class' => EtatDevis::class,
            'columns' => [
                'label' => 'etatdevis_nom'
            ]
        ]
    ];

    /**
     * Méthodes d'ajout, lecture, édition et suppression des modules liés aux devis
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function moduleAction($id = null)
    {
        if ($this->request->isPost()
            && is_null($id)) {

            return $this->addModuleDevis();

        } elseif ($this->request->isPost()
            && isset($id)) {

            return $this->editModuleDevis($id);

        } elseif ($this->request->isDelete()
            && isset($id)) {

            return $this->deleteModuleDevis($id);

        } elseif ($this->request->isGet()
            && isset($id)) {

            return $this->getModuleDevis($id);
        }

        return [];
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Devis $model
     */
    protected function fetchSearch($model): array
    {
        $result = parent::fetchSearch($model);

        /** @var Projet $projet */
        $projet = $model->getProjet();

        /** @var Module[] $modules */
        $modules = $model->getModule();

        $result['projet_nom_reference'] = $projet->getNom() . ' ' . $projet->getReference();

        $result['module'] = [];

        foreach ($modules as $module) {

            $result['module'][] = $module->toArray(['id', 'id_gamme', 'id_nature', 'nom', 'marge', 'description']);
        }

        return $result;
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('reference LIKE :reference:', ['reference' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Devis $model
     */
    protected function beforeCreate(ModelBase $model)
    {
        $model->setIdProjet($this->request->getPost('id_projet', Filter::FILTER_ABSINT, $model->getIdProjet()));

        $this->commonBefore($model);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Devis $model
     *
     */
    protected function beforeUpdate(ModelBase $model)
    {
        $this->commonBefore($model);
    }

    /**
     * Méthode d'unification des setters d'api
     *
     * @param ModelBase|Devis $model
     */
    private function commonBefore(ModelBase $model)
    {
        $model->setIdEtat($this->request->getPost('id_etat', Filter::FILTER_ABSINT, $model->getIdEtat()));
        $model->setDateFinValidite($this->request->getPost('date_fin_validite', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getDateFinValidite()));
        $model->setNotePublique($this->request->getPost('note_publique', Filter::FILTER_STRIPTAGS, $model->getNotePublique()));
        $model->setNotePrivee($this->request->getPost('note_privee', Filter::FILTER_STRIPTAGS, $model->getNotePrivee()));
        $model->setRemise($this->request->getPost('remise', Filter::FILTER_FLOAT_CAST, $model->getRemise()));
    }

    /**
     * Méthode d'ajout d'un nouveau module à un devis
     *
     * @return array
     * @throws \Exception
     */
    private function addModuleDevis()
    {
        $module = new Module;
        $module->setIdDevis($this->request->getPost('id_devis', Filter::FILTER_INT_CAST));
        $module->setIsModele(false);
        $this->commonModuleDevis($module);

        if ($module->create() !== false) {

            $this->response->setStatusCode(201, 'Created');

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $module->toArray()
            ];
        }

        $this->response->setStatusCode(409, 'Conflict');

        return [
            'status' => 'FAILED',
            'messages' => Helper::getMessages($module)
        ];
    }

    /**
     * Retourne la liste des modules liés à un devis
     *
     * @param $moduleId
     * @return array
     * @throws \Exception
     */
    private function getModuleDevis($moduleId)
    {
        $moduleId = $this->filter->sanitize($moduleId, Filter::FILTER_ABSINT);

        /** @var Module $module */
        $module = Module::findFirst($moduleId);

        if ($module !== false) {

            $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

            return [
                'status' => 'SUCCESS',
                'latency' => round((microtime(true) - $requestTime) * 1000),
                'data' => $module->toArray(['id', 'id_gamme', 'id_nature', 'nom', 'marge', 'description'])
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Méthode de modification d'un module lié à un devis
     *
     * @param $moduleId
     * @return array
     * @throws \Exception
     */
    private function editModuleDevis($moduleId)
    {
        /** @var Module $module */
        $module = Module::findFirst($moduleId);

        if ($module !== false) {

            $this->commonModuleDevis($module);

            if ($module->update() !== false) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'data' => $this->afterUpdate($module)
                ];
            }

            $this->response->setStatusCode(409, 'Conflict');

            return [
                'status' => 'FAILED',
                'messages' => Helper::getMessages($module)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Supprime un module lié à un devis
     *
     * @param $moduleId
     * @return array
     * @throws \Exception
     */
    private function deleteModuleDevis($moduleId)
    {
        $moduleIdFiltered = $this->filter->sanitize($moduleId, Filter::FILTER_ABSINT);

        /** @var Module $module */
        $module = Module::findFirst($moduleIdFiltered);

        if ($module !== false) {

            if ($module->delete()) {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'message' => "La ressource numéro " . $module->getId() . " à été supprimée !"
                ];
            }

            $this->response->setStatusCode(409, 'Conflict');

            return [
                'status' => 'FAILED',
                'messages' => Helper::getMessages($module)
            ];
        }

        return [
            'status' => 'ERROR',
            'message' => $this->notFoundMessage()
        ];
    }

    /**
     * Méthode commune de modification d'un module de devis
     *
     * @param Module $model
     */
    private function commonModuleDevis($model)
    {
        $model->setIdGamme($this->request->getPost('id_gamme', Filter::FILTER_INT_CAST, $model->getIdGamme()));
        $model->setIdNature($this->request->getPost('id_nature', Filter::FILTER_INT_CAST, $model->getIdNature()));
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setMarge($this->request->getPost('marge', Filter::FILTER_FLOAT_CAST, $model->getMarge()));
        $model->setDescription($this->request->getPost('description', Filter::FILTER_STRIPTAGS, $model->getDescription()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchDevisApi
     * @apiGroup DevisApi
     * @apiVersion 0.0.1
     * @api {post} /devis/search Recherche des éléments
     * @apiPermission {role} | Devis | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateDevisApi
     * @apiGroup DevisApi
     * @apiVersion 0.0.1
     * @api {post} /devis Ajoute un nouvel élément
     * @apiPermission {role} | Devis | create
     * @apiUse CreateGenericApi
     * @apiUse DataDevisApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadDevisApi
     * @apiGroup DevisApi
     * @apiVersion 0.0.1
     * @api {get} /devis/:id Affiche un élément
     * @apiPermission {role} | Devis | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateDevisApi
     * @apiGroup DevisApi
     * @apiVersion 0.0.1
     * @api {post} /devis/:id Modifie un élément
     * @apiPermission {role} | Devis | update
     * @apiUse UpdateGenericApi
     * @apiUse DataDevisApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteDevisApi
     * @apiGroup DevisApi
     * @apiVersion 0.0.1
     * @api {delete} /devis/:id Supprime un élément
     * @apiPermission {role} | Devis | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
