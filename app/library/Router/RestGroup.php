<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Router;

use Phalcon\Mvc\Router\Group;

/**
 * Class RestGroup
 *
 * Groupe de gestion des routes REST pour l'application
 *
 * @package App\Library\Router
 */
class RestGroup extends Group
{
    /**
     * @inheritdoc
     */
    public function initialize()
    {
        // Définition du chemin des controlleurs
        $this->setPaths(
            [
                'namespace'  => 'App\Controller'
            ]
        );

        $this->add('/:controller', [
            'controller' => 1
        ]);

        $this->add('/:controller/:action', [
            'controller' => 1,
            'action'     => 2
        ]);

        $this->add('/:controller/:action/:params', [
            'controller' => 1,
            'action'     => 2,
            'params'     => 3
        ]);

        // Route d'accès à la liste des items (Scrud)
        $this->add('/:controller/search', [
            'controller' => 1,
            'action'     => 'search'
        ])->via(
            [
                'GET',
                'POST'
            ]
        );

        // Route d'accès à la création d'un item (sCrud)
        $this->addPost('/:controller', [
            'controller' => 1,
            'action'     => 'create'
        ]);

        // Route d'accès à la lecture d'un item (scRud)
        $this->addGet('/:controller/([0-9]+)', [
            'controller' => 1,
            'action'     => 'read',
            'id'         => 2
        ]);

        // Route d'accès à la mise à jour d'un item (scrUd)
        $this->add('/:controller/([0-9]+)', [
            'controller' => 1,
            'action'     => 'update',
            'id'         => 2
        ])->via(
            [
                'PUT',
                'POST'
            ]
        );

        // Route d'accès à la suppression d'un item (scruD)
        $this->addDelete('/:controller/([0-9]+)', [
            'controller' => 1,
            'action'     => 'delete',
            'id'         => 2
        ]);

        // Route par défaut pour l'accès à la connexion
        $this->add('/auth', [
            'controller' => 'auth',
            'action'     => 'login'
        ]);

        // Route d'accès aux méthodes du controlleur authentification
        $this->add('/auth/:action', [
            'controller' => 'auth',
            'action'     => 1
        ]);

        // Route d'accès aux méthodes avec paramètres du controlleur d'authentification
        $this->add('/auth/:action/:params', [
            'controller' => 'auth',
            'action'     => 1,
            'params'     => 2
        ]);
    }
}
