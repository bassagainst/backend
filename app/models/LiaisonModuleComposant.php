<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Builder\Project\Simple;

/**
 * Class LiaisonModuleComposant
 * @package App\Model
 *
 * @property Simple|Composant[]        $composant
 * @property Simple|Module[]           $module
 *
 * @method   Simple|Composant[]        getComposant($parameters = null)
 * @method   Simple|Module[]           getModule($parameters = null)
 *
 * @method   integer                   countComposant()
 * @method   integer                   countModule()
 */
class LiaisonModuleComposant extends ModelBase
{
    /** @var int TTL_CACHE */
    const TTL_CACHE = -1;

    /** @var integer $id_composant */
    protected $id_composant;

    /** @var integer $id_composant */
    protected $id_module;

    /** @var integer $id_composant */
    protected $quantite;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Liaison_Module_Composant");

        $this->belongsTo('id_composant', 'App\Model\Composant', 'id',
            [
                'alias' => 'Composant',
                'foreignKey' => [
                    'message' => "Le composant choisi n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_module', 'App\Model\Module', 'id',
            [
                'alias' => 'Module',
                'foreignKey' => [
                    'message' => "Le module choisi n'existe pas"
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Liaison_Module_Composant';
    }

    /**
     * Getter pour l'id du composant
     *
     * @return int
     */
    public function getIdComposant()
    {
        return (int) $this->id_composant;
    }

    /**
     * Setter pour l'id du composant
     *
     * @param int $id_composant
     */
    public function setIdComposant(int $id_composant)
    {
        $this->id_composant = $id_composant;
    }

    /**
     * Getter pour l'id du module
     *
     * @return int
     */
    public function getIdModule()
    {
        return (int) $this->id_module;
    }

    /**
     * Setter pour l'id du module
     *
     * @param int $id_module
     */
    public function setIdModule(int $id_module)
    {
        $this->id_module = $id_module;
    }

    /**
     * Getter pour la quantité
     *
     * @return int
     */
    public function getQuantite()
    {
        return (int) $this->quantite;
    }

    /**
     * Setter pour la quantité
     *
     * @param int $quantite
     */
    public function setQuantite(int $quantite)
    {
        $this->quantite = $quantite;
    }
}
