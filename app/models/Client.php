<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Helper;
use App\Library\Model\ModelBase;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

/**
 * Class Client
 * @package App\Model
 */
class Client extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $nom */
    protected $nom;

    /** @var string $prenom */
    protected $prenom;

    /** @var string $numero_rue */
    protected $numero_rue;

    /** @var string $rue */
    protected $rue;

    /** @var string $ville */
    protected $ville;

    /** @var string $cp */
    protected $cp;

    /** @var string $email */
    protected $email;

    /** @var string $tel */
    protected $tel;

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'vous devez entrer une adresse email valide',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Client");

        $this->hasMany('id', 'App\Model\Projet', 'id_client',
            [
                'alias'      => 'Projet',
                'foreignKey' => true
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Client';
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Getter pour le prénom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Setter pour le prénom
     *
     * @param string $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Getter pour le numéro de la rue
     *
     * @return string
     */
    public function getNumeroRue()
    {
        return $this->numero_rue;
    }

    /**
     * Setter pour le numéro de la rue
     *
     * @param string $numero_rue
     */
    public function setNumeroRue(string $numero_rue)
    {
        $this->numero_rue = $numero_rue;
    }

    /**
     * Getter pour la rue
     *
     * @return string
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * Setter pour la rue
     *
     * @param string $rue
     */
    public function setRue(string $rue)
    {
        $this->rue = $rue;
    }

    /**
     * Getter pour la ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Setter pour la ville
     *
     * @param string $ville
     */
    public function setVille(string $ville)
    {
        $this->ville = $ville;
    }

    /**
     * Getter pour le code postal
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Setter pour le code postal
     *
     * @param string $cp
     */
    public function setCp(string $cp)
    {
        $this->cp = $cp;
    }

    /**
     * Getter pour l'adresse email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Setter pour l'adresse email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Getter pour le téléphone
     *
     * @return string|null
     * @throws \Exception
     */
    public function getTel()
    {
        return Helper::formatTelephone($this->tel);
    }

    /**
     * Setter pour le téléphone
     *
     * @param string $tel
     * @throws \Exception
     */
    public function setTel($tel)
    {
        $this->tel = Helper::storeTelephone($tel);
    }
}
