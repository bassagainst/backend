<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Helper;
use App\Library\Model\ModelBase;
use App\Library\Validator\PasswordStrength as PasswordStrengthValidator;
use Phalcon\Acl\AdapterInterface;
use Phalcon\Di;
use Phalcon\Mvc\Model\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Confirmation as ConfirmationValidator;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

/**
 * Class Utilisateur
 * @package App\Model
 */
class Utilisateur extends ModelBase
{
    /** @var int TTL_CACHE */
    const TTL_CACHE = 86400;

    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $nom */
    protected $nom;

    /** @var string $prenom */
    protected $prenom;

    /** @var string $email */
    protected $email;

    /** @var string $tel */
    protected $tel;

    /** @var string $mdp */
    protected $mdp;

    /** @var string $role */
    protected $role;

    /** @var string $mdpOrigin */
    protected $mdpOrigin;

    /** @var string $confMdpOrigin */
    protected $confMdpOrigin;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Utilisateur");

        $this->hasMany('id', 'App\Model\Historique', 'id_utilisateur',
            [
                'alias' => 'Historique'
            ]
        );

        $this->hasMany('id', 'App\Model\Projet', 'id_utilisateur',
            [
                'alias' => 'Projet',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('role', 'App\Model\Roles', 'name',
            [
                'alias' => 'Roles',
                'foreignKey' => [
                    'message' => "Le rôle choisi pour cet utilisateur n'existe pas"
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Utilisateur';
    }

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function validation()
    {
        $validator = new Validation;

        $validator->add(
            'email',
            new UniquenessValidator(
                [
                    'model' => $this,
                    'message' => "l'adresse email de l'utilisateur doit être unique"
                ]
            )
        );

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model' => $this,
                    'message' => 'vous devez entrer une adresse email valide'
                ]
            )
        );

        if (isset($this->mdpOrigin)) {

            $validator->add(
                'mdpOrigin',
                new PasswordStrengthValidator(
                    [
                        'model' => $this,
                        'message' => 'le mot de passe ne respecte pas les consignes de sécurité'
                    ]
                )
            );

            $validator->add(
                'mdpOrigin',
                new ConfirmationValidator(
                    [
                        'model' => $this,
                        'with' => 'confMdpOrigin',
                        'message' => 'les mot de passes ne correspondent pas'
                    ]
                )
            );
        }

        return $this->validate($validator);
    }

    /**
     * Définit un nouveau mot de passe pour l'utilisateur
     *
     * @param $mdp
     * @param $confMdpOrigin
     */
    public function setMdp($mdp, $confMdpOrigin)
    {
        if (isset($mdp)) {

            $this->mdpOrigin = $mdp;
            $this->confMdpOrigin = $confMdpOrigin;

            $this->mdp = $this->getDI()->get('security')->hash($mdp);
        }
    }

    /**
     * @param $mdp
     * @return bool
     */
    public function checkMdp($mdp): bool
    {
        return $this->getDI()->get('security')->checkHash($mdp, $this->mdp);
    }

    /**
     * @return string
     */
    public function getPayload(): string
    {
        /** @var \Phalcon\Config $security */
        $security = $this->getDI()->get('config')->get('security');

        /** @var AdapterInterface $acl */
        $acl = $this->getDI()->get('acl');

        $exp = new \DateTime();
        @$exp->modify('+' . $security->get('jwtExpire'));

        $data = [
            'payload' => [
                'id_utilisateur' => $this->getId(),
                'email' => $this->getEmail(),
                'nom' => $this->getNom(),
                'prenom' => $this->getPrenom(),
                'role' => $this->getRole(),
                'acl' => [
                    'admin' => [
                        'utilisateurs' => $acl->isAllowed($this->getRole(), 'Utilisateur', 'search'),
                        'acls' => $acl->isAllowed($this->getRole(), 'Acl', 'search')
                    ],
                    'gestion' => [
                        'fournisseurs' => $acl->isAllowed($this->getRole(), 'Fournisseur', 'search'),
                        'clients' => $acl->isAllowed($this->getRole(), 'Client', 'search')
                    ],
                    'settings' => [
                        'gammes' => $acl->isAllowed($this->getRole(), 'Gamme', 'search'),
                        'typegammes' => $acl->isAllowed($this->getRole(), 'TypeGamme', 'search'),
                        'typefinitions' => $acl->isAllowed($this->getRole(), 'TypeFinition', 'search'),
                        'typeisolants' => $acl->isAllowed($this->getRole(), 'TypeIsolant', 'search'),
                        'modules' => $acl->isAllowed($this->getRole(), 'Module', 'search'),
                        'naturemodules' => $acl->isAllowed($this->getRole(), 'NatureModule', 'search'),
                        'composants' => $acl->isAllowed($this->getRole(), 'Composant', 'search'),
                        'famillecomposants' => $acl->isAllowed($this->getRole(), 'FamilleComposant', 'search'),
                        'etatdevis' => $acl->isAllowed($this->getRole(), 'EtatDevis', 'search'),
                        'tva' => $acl->isAllowed($this->getRole(), 'Tva', 'search')
                    ],
                    'conception' => [
                        'projets' => $acl->isAllowed($this->getRole(), 'Projet', 'search'),
                        'devis' => $acl->isAllowed($this->getRole(), 'Devis', 'search')
                    ],
                    'profil' => $acl->isAllowed($this->getRole(), 'Utilisateur', 'profil')
                ]
            ],
            'iat' => time(),
            'exp' => $exp->getTimestamp()
        ];

        return Helper::encodeJwt($data);
    }

    /**
     * Retourne le <refreshToken> de l'utilisateur
     *
     * @return string
     * @throws \Exception
     */
    public function getRefreshPayload(): string
    {
        /** @var \Phalcon\Config $security */
        $security = $this->getDI()->get('config')->get('security');

        $exp = new \DateTime;
        @$exp->modify('+' . $security->get('jwtRefresh'));

        $expire = $exp->getTimestamp() - (new \DateTime)->getTimestamp();

        $data = [
            'payload' => [
                'token' => Helper::generateUuid($this, $expire)
            ],
            'iat' => (new \DateTime)->getTimestamp(),
            'exp' => $exp->getTimestamp()
        ];

        return Helper::encodeJwt($data);
    }

    /**
     * Demande a l'application de régénérer le token
     *
     * Retourne un nouveau token uniquement si le refreshToken est valide, rafraichit
     * le refreshToken par référence si celui-ci dépasse la date de renouvellement
     *
     * @param $refreshToken
     * @return string|bool
     * @throws \Exception
     */
    public static function refreshPayload(&$refreshToken)
    {
        /** @var \Redis $redis */
        $redis = Di::getDefault()->get('redis');

        /** @var \Phalcon\Config $security */
        $security = Di::getDefault()->get('config')->get('security');

        /** @var AdapterInterface $acl */
        $acl = Di::getDefault()->get('acl');

        try {

            $data = Helper::decodeJwt($refreshToken);

        } catch (\UnexpectedValueException $e) {

            return false;

        } catch (\Exception $e) {

            return false;
        }

        $tokenPayload = $redis->get($data->payload->token);

        if ($tokenPayload !== false) {

            $utilisateur = self::findFirst(
                [
                    'conditions' => 'id = ?1',
                    'bind' => [
                        1 => json_decode($tokenPayload)->userId ?? null,
                    ]
                ]
            );

            // Recherche l'utilisateur du token, si le token n'existe pas, retourne false
            if ($utilisateur !== false && $utilisateur instanceof Utilisateur) {

                $time = new \DateTime();
                @$time->setTimestamp($data->iat);
                @$time->modify('+' . $security->get('jwtRenew'));

                $exp = new \DateTime();
                $exp->modify('+' . $security->get('jwtExpire'));

                // Régénère le TLL Redis si le token est à <jwtExpire> de son temps de vie
                if (isset($data->exp) && time() >= $time->getTimestamp()) {

                    $renew = new \DateTime();
                    $renew->modify('+' . $security->get('jwtRefresh'));

                    $expire = $renew->getTimestamp() - (new \DateTime)->getTimestamp();

                    $redis->setTimeout($data->payload->token, $expire);

                    $dataRefresh = [
                        'payload' => [
                            'token' => $data->payload->token
                        ],
                        'iat' => (new \DateTime)->getTimestamp(),
                        'exp' => $renew->getTimestamp()
                    ];

                    $refreshToken = Helper::encodeJwt($dataRefresh);
                }

                $response = [
                    'payload' => [
                        'id_utilisateur' => $utilisateur->getId(),
                        'email' => $utilisateur->email,
                        'nom' => $utilisateur->nom,
                        'prenom' => $utilisateur->prenom,
                        'role' => $utilisateur->role,
                        'acl' => [
                            'admin' => [
                                'utilisateurs' => $acl->isAllowed($utilisateur->getRole(), 'Utilisateur', 'search'),
                                'acls' => $acl->isAllowed($utilisateur->getRole(), 'Acl', 'search')
                            ],
                            'gestion' => [
                                'fournisseurs' => $acl->isAllowed($utilisateur->getRole(), 'Fournisseur', 'search'),
                                'clients' => $acl->isAllowed($utilisateur->getRole(), 'Client', 'search')
                            ],
                            'settings' => [
                                'gammes' => $acl->isAllowed($utilisateur->getRole(), 'Gamme', 'search'),
                                'typegammes' => $acl->isAllowed($utilisateur->getRole(), 'TypeGamme', 'search'),
                                'typefinitions' => $acl->isAllowed($utilisateur->getRole(), 'TypeFinition', 'search'),
                                'typeisolants' => $acl->isAllowed($utilisateur->getRole(), 'TypeIsolant', 'search'),
                                'modules' => $acl->isAllowed($utilisateur->getRole(), 'Module', 'search'),
                                'naturemodules' => $acl->isAllowed($utilisateur->getRole(), 'NatureModule', 'search'),
                                'composants' => $acl->isAllowed($utilisateur->getRole(), 'Composant', 'search'),
                                'famillecomposants' => $acl->isAllowed($utilisateur->getRole(), 'FamilleComposant', 'search'),
                                'etatdevis' => $acl->isAllowed($utilisateur->getRole(), 'EtatDevis', 'search'),
                                'tva' => $acl->isAllowed($utilisateur->getRole(), 'Tva', 'search')
                            ],
                            'conception' => [
                                'projets' => $acl->isAllowed($utilisateur->getRole(), 'Projet', 'search'),
                                'devis' => $acl->isAllowed($utilisateur->getRole(), 'Devis', 'search')
                            ],
                            'profil' => $acl->isAllowed($utilisateur->getRole(), 'Utilisateur', 'profil')
                        ]
                    ],
                    'iat' => time(),
                    'exp' => $exp->getTimestamp()
                ];

                return Helper::encodeJwt($response);
            }
        }

        return false;
    }

    /**
     * Getter de nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter de nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = strtoupper($nom);
    }

    /**
     * Getter de prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Setter de prenom
     *
     * @param string $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Getter d'email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Setter d'email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Getter pour le téléphone
     *
     * @return string|null
     * @throws \Exception
     */
    public function getTel()
    {
        return Helper::formatTelephone($this->tel);
    }

    /**
     * Setter pour le téléphone
     *
     * @param string $tel
     * @throws \Exception
     */
    public function setTel($tel)
    {
        try {

            $this->tel = Helper::storeTelephone($tel);

        } catch (\Exception $e) {

            $this->appendMessage(new Message("Le numéro de téléphone n'est pas valide", 'tel'));
        }
    }

    /**
     * Getter pour le rôle
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Setter pour le rôle
     *
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }
}
