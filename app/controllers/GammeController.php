<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\Gamme;
use App\Model\TypeGamme;
use App\Model\TypeFinition;
use App\Model\TypeIsolant;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class GammeController
 * @package App\Controller
 *
 * @apiDefine DataGammeApi
 * @apiParam {Number} id_type_gamme Identifiant du type de gamme
 * @apiParam {Number} id_type_finition Identifiant du type de finition de la gamme
 * @apiParam {Number} id_type_isolant Identifiant du type d'isolant de la gamme
 * @apiParam {String} nom Nom de la gamme
 * @apiParam {String} qualite_huisserie Qualité des huisseries de la gamme
 * @apiParam {String} description Description de la gamme
 */
class GammeController extends ControllerApi
{
    /** @var string $model */
    protected $model = Gamme::class;

    /** @var array $columns */
    protected $columns = ['id', 'qualite_huisserie', 'nom', 'id_type_gamme',
        'id_type_finition', 'id_type_isolant', 'description'];

    /** @var array $join */
    protected $join = [
        [
            'class' => TypeGamme::class,
            'columns' => [
                'label' => 'typegamme_nom'
            ]
        ],
        [
            'class' => TypeFinition::class,
            'columns' => [
                'label' => 'typefinition_nom'
            ]
        ],
        [
            'class' => TypeIsolant::class,
            'columns' => [
                'label' => 'typeisolant_nom'
            ]
        ]
    ];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Gamme $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setIdTypeGamme($this->request->getPost('id_type_gamme', Filter::FILTER_ABSINT, $model->getIdTypeGamme()));
        $model->setIdTypeFinition($this->request->getPost('id_type_finition', Filter::FILTER_ABSINT, $model->getIdTypeFinition()));
        $model->setIdTypeIsolant($this->request->getPost('id_type_isolant', Filter::FILTER_ABSINT, $model->getIdTypeIsolant()));
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setQualiteHuisserie($this->request->getPost('qualite_huisserie', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getQualiteHuisserie()));
        $model->setDescription($this->request->getPost('description', Filter::FILTER_STRIPTAGS, $model->getDescription()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchGammeApi
     * @apiGroup GammeApi
     * @apiVersion 0.0.1
     * @api {post} /gamme/search Recherche des éléments
     * @apiPermission {role} | Gamme | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateGammeApi
     * @apiGroup GammeApi
     * @apiVersion 0.0.1
     * @api {post} /gamme Ajoute un nouvel élément
     * @apiPermission {role} | Gamme | create
     * @apiUse CreateGenericApi
     * @apiUse DataGammeApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadGammeApi
     * @apiGroup GammeApi
     * @apiVersion 0.0.1
     * @api {get} /gamme/:id Affiche un élément
     * @apiPermission {role} | Gamme | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateGammeApi
     * @apiGroup GammeApi
     * @apiVersion 0.0.1
     * @api {post} /gamme/:id Modifie un élément
     * @apiPermission {role} | Gamme | update
     * @apiUse UpdateGenericApi
     * @apiUse DataGammeApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteGammeApi
     * @apiGroup GammeApi
     * @apiVersion 0.0.1
     * @api {delete} /gamme/:id Supprime un élément
     * @apiPermission {role} | Gamme | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
