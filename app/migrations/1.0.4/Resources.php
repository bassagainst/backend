<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ResourcesMigration_104
 */
class ResourcesMigration_104 extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     * @throws \Phalcon\Db\Exception
     */
    public function up()
    {
        self::$connection->query('SET GLOBAL FOREIGN_KEY_CHECKS=0')->execute();
        $this->morphTable('Resources', [
                'columns' => [
                    new Column(
                        'name',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 32,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'description',
                        [
                            'type' => Column::TYPE_TEXT,
                            'after' => 'name'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['name'], 'PRIMARY')
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'latin1_swedish_ci'
                ],
            ]
        );
        self::$connection->query('SET GLOBAL FOREIGN_KEY_CHECKS=1')->execute();
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }
}
