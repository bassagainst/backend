<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

use App\Library\Helper;
use App\Model\Utilisateur;
use App\Seeds\UtilisateurSeed;

/**
 * Class UtilisateurTest
 */
class UtilisateurTest extends \UnitTestCase
{
    /**
     * Tâche de création d'un Utilisateur
     * @throws Exception
     */
    public function testCreate()
    {
        self::message($this, "Lancement de la tâche de création...");

        for ($i = 0; $i < rand(1, self::RAND_GENERATION); $i++) {

            $utilisateur = new UtilisateurSeed;

            self::message($this, "Boucle : Tentative de création de l'utilisateur[" . ($i +1) . ']');
            $this->assertTrue(
                $utilisateur->save(),
                implode(", ", Helper::getMessages($utilisateur))
            );
        }

        self::message($this, "Fin de la tâche de création..." . PHP_EOL);
    }

    /**
     * Tâche de suppression d'un Utilisateur
     */
    public function testDelete()
    {
        self::message($this, "Lancement de la tâche de suppression...");

        $count = Utilisateur::count();

        for ($i = 0; $i < rand(1, $count -1); $i++) {

            /** @var Utilisateur $utilisateur */
            $utilisateur = Utilisateur::findFirst(
                [
                    'offset' => rand(0, $count - 1 - $i)
                ]
            );

            self::message($this, "Boucle : Vérification de l'instance retournée[" . ($i +1) . ']');
            $this->assertInstanceOf(
                Utilisateur::class,
                $utilisateur,
                "Instance utilisateur is not OK"
            );

            self::message($this, "Boucle : Tentative de suppression de l'utilisateur[" . ($i +1) . ']');
            $this->assertTrue($utilisateur->delete(), "User not deleted");
        }

        self::message($this, "Fin de la tâche de suppression..." . PHP_EOL);
    }
}
