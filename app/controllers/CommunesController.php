<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerBase;
use App\Library\Plugin\SecurityPlugin;
use App\Library\Utils\RequestDataGouv;
use Phalcon\Filter;
use Phalcon\Http\Client\Exception;

/**
 * Class DataGouvController
 * @package App\Controller
 */
class CommunesController extends ControllerBase
{
    public function beforeExecuteRoute()
    {
        $security = new SecurityPlugin;

        if ($security->authenticate()) {

            return true;

        } else {

            $this->response->setStatusCode(401, 'Unauthorized');

            $this->response->setJsonContent(
                [
                    'status' => 'FAILED',
                    'message' => "L'authentification à échouée ou expirée !"
                ]
            );

            return false;
        }
    }

    /**
     * Recherche par code postal
     *
     * @param null $query
     * @return array
     *
     * @apiName CpCommunesApi
     * @apiGroup CommunesApi
     * @apiVersion 0.0.1
     * @api {post} /communes/rechercheCp/:query Affiche les communes par code postaux
     * @apiPermission {role} | Communes | rechercheCp
     * @apiParam {String} query Recherche par codes postaux
     *
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Object} data Retourne la liste des commnues
     * @apiError Forbidden L'utilisateur n'est pas autorisé à rejoindre l'application
     * @apiError NotFound La recherche n'a rien donner
     */
    public function rechercheCpAction($query = null)
    {
        $cp = $this->filter->sanitize($query, Filter::FILTER_STRIPTAGS);

        try {
            $response = RequestDataGouv::send('communes',
                [
                    'codePostal' => $cp,
                    'fields'     => 'nom,codesPostaux',
                    'format'     => 'json',
                    'geometry'   => 'centre'
                ]
            );

        } catch (Exception $e) {

            return [
                'status'  => 'ERROR',
                'message' => $e->getMessage()
            ];
        }

        return [
            'status' => 'SUCCESS',
            'data'   => json_decode($response->body)
        ];
    }

    /**
     * Recherche par ville
     *
     * @param null $query
     * @return array
     *
     * @apiName VilleCommunesApi
     * @apiGroup CommunesApi
     * @apiVersion 0.0.1
     * @api {post} /communes/rechercheVille/:query Affiche les communes par villes
     * @apiPermission {role} | Communes | rechercheVille
     * @apiParam {String} query Recherche par villes
     *
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {Object} data Retourne la liste des commnues
     * @apiError Forbidden L'utilisateur n'est pas autorisé à rejoindre l'application
     * @apiError NotFound La recherche n'a rien donner
     */
    public function rechercheVilleAction($query = null)
    {
        $ville = $this->filter->sanitize($query, Filter::FILTER_STRIPTAGS);

        try {
            $response = RequestDataGouv::send('communes',
                [
                    'nom'      => $ville,
                    'fields'   => 'nom,codesPostaux',
                    'format'   => 'json',
                    'geometry' => 'centre'
                ]
            );

        } catch (Exception $e) {

            return [
                'status'  => 'ERROR',
                'message' => $e->getMessage()
            ];
        }

        return [
            'status' => 'SUCCESS',
            'data'   => json_decode($response->body)
        ];
    }
}
