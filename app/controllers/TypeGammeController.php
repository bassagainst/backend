<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\TypeGamme;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class TypeGammeController
 * @package App\Controller
 *
 * @apiDefine DataTypeGammeApi
 * @apiParam {String} label Label du type de gamme
 * @apiParam {Number} ordre Ordre du type de gamme
 */
class TypeGammeController extends ControllerApi
{
    /** @var string $model */
    protected $model = TypeGamme::class;

    /** @var array $columns */
    protected $columns = ['id', 'label', 'ordre'];

    /**
     * @param Criteria $criteria
     */
    public function filters(Criteria $criteria)
    {
        $criteria->orderBy('ordre');
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('label LIKE :label:', ['label' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|TypeGamme $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setLabel($this->request->getPost('label', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getLabel()));
        $model->setOrdre($this->request->getPost('ordre', Filter::FILTER_ABSINT, $model->getOrdre()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchTypeGammeApi
     * @apiGroup TypeGammeApi
     * @apiVersion 0.0.1
     * @api {post} /type-gamme/search Recherche des éléments
     * @apiPermission {role} | TypeGamme | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateTypeGammeApi
     * @apiGroup TypeGammeApi
     * @apiVersion 0.0.1
     * @api {post} /type-gamme Ajoute un nouvel élément
     * @apiPermission {role} | TypeGamme | create
     * @apiUse CreateGenericApi
     * @apiUse DataTypeGammeApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadTypeGammeApi
     * @apiGroup TypeGammeApi
     * @apiVersion 0.0.1
     * @api {get} /type-gamme/:id Affiche un élément
     * @apiPermission {role} | TypeGamme | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateTypeGammeApi
     * @apiGroup TypeGammeApi
     * @apiVersion 0.0.1
     * @api {post} /type-gamme/:id Modifie un élément
     * @apiPermission {role} | TypeGamme | update
     * @apiUse UpdateGenericApi
     * @apiUse DataTypeGammeApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteTypeGammeApi
     * @apiGroup TypeGammeApi
     * @apiVersion 0.0.1
     * @api {delete} /type-gamme/:id Supprime un élément
     * @apiPermission {role} | TypeGamme | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
