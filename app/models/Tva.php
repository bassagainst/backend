<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Helper;
use App\Library\Model\ModelBase;

/**
 * Class Tva
 * @package App\Model
 */
class Tva extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $label */
    protected $label;

    /** @var double $taux */
    protected $taux;

    /** @var string $date_debut_validite */
    protected $date_debut_validite;

    /** @var string $date_fin_validite */
    protected $date_fin_validite;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Tva");

        $this->hasMany('id', 'App\Model\Composant', 'id_tva',
            [
                'alias'      => 'Composant',
                'foreignKey' => true
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Tva';
    }

    /**
     * Getter pour le label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Setter pour le label
     *
     * @param string $label
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * Getter pour le taux
     *
     * @return double
     */
    public function getTaux()
    {
        return (double) $this->taux;
    }

    /**
     * Setter pour le taux
     *
     * @param float $taux
     */
    public function setTaux(float $taux)
    {
        $this->taux = $taux;
    }

    /**
     * Getter pour date de début de validité
     *
     * @return string|null
     */
    public function getDateDebutValidite()
    {
        return is_string($this->date_debut_validite)
            ? (new \DateTime($this->date_debut_validite))->format('d/m/Y') ?? null
            : null;
    }

    /**
     * Setter pour date de début de validité
     *
     * @param string $date_debut_validite
     */
    public function setDateDebutValidite(string $date_debut_validite)
    {
        $this->date_debut_validite = Helper::validateDate($date_debut_validite, 'd/m/Y')
            ? \DateTime::createFromFormat('d/m/Y', $date_debut_validite)->format('Y-m-d')
            : null;
    }

    /**
     * Getter pour date de fin de validité
     *
     * @return string|null
     */
    public function getDateFinValidite()
    {
        return is_string($this->date_fin_validite)
            ? (new \DateTime($this->date_fin_validite))->format('d/m/Y') ?? null
            : null;
    }

    /**
     * Setter pour date de fin de validité
     *
     * @param string $date_fin_validite
     */
    public function setDateFinValidite(string $date_fin_validite)
    {
        $this->date_fin_validite = Helper::validateDate($date_fin_validite, 'd/m/Y')
            ? \DateTime::createFromFormat('d/m/Y', $date_fin_validite)->format('Y-m-d')
            : null;
    }

}
