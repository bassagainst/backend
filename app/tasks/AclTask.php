<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Task;

use App\Library\Controller\ControllerSecurity;
use App\Library\Task\TaskBase;
use Phalcon\Acl\Adapter\Database;
use Phalcon\Acl\AdapterInterface;

/**
 * Class AdminTask
 * @package App\Task
 */
class AclTask extends TaskBase
{
    /**
     * Default way
     */
    public function mainAction()
    {
        self::nl("Tâches de gestion des ACLs de l'application,");
        self::nl("Voici la liste des commandes disponibles : ");

        self::listMethods($this);
    }

    public function rolesAction()
    {
        /** @var AdapterInterface $acl */
        $acl = $this->getDI()->get('acl');

        $acl->addRole('Administrateur');

        die(var_dump($acl->getRoles()));
    }

    public function generateAction()
    {
        /** @var \Phalcon\Db\AdapterInterface $db */
        $db = $this->getDI()->getShared('db');

        $db->delete('Resources');
        $db->delete('Resources_Accesses');

        /** @var Database $acl */
        $acl = $this->getDI()->get('acl');

        /** @var \Phalcon\Config $config */
        $config = $this->getDI()->getShared('config')->get('application');

        $files = new \DirectoryIterator($config->get('controllersDir'));

        foreach ($files as $file) {

            if ($file->isFile() && strstr($file->getFilename(), 'Controller')) {

                $reflection = '\\App\\Controller\\' . $file->getBasename('.php');

                try {

                    $controller = new $reflection;

                    if ($controller instanceof ControllerSecurity) {

                        $controller = new \ReflectionClass($reflection);

                        $controllerName = strstr($controller->getShortName(), 'Controller', true);

                        $acl->addResource($controllerName);

                        foreach ($controller->getMethods() as $methods) {

                            if ($methods->isPublic() && strstr($methods->getName(), 'Action')) {

                                $actionName = strstr($methods->getName(), 'Action', true);

                                $acl->addResourceAccess($controllerName, $actionName);
                            }
                        }

                        self::nl('.', null, null, false);
                    }

                } catch (\ReflectionException $e) {

                    echo $e->getMessage();
                    exit(255);

                } catch (\Phalcon\Acl\Exception $e) {

                    echo $e->getMessage();
                    exit(255);
                }
            }
        }

        self::nl(PHP_EOL . "Les ressources et les accès ont été réinitialisés !");
    }
}
