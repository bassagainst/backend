<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Helper;
use App\Library\Model\ModelBase;
use Phalcon\Builder\Project\Simple;
use Phalcon\Security;

/**
 * Class Devis
 * @package App\Model
 *
 * @property Simple|Module[]           $module
 * @property Simple|Projet[]           $projet
 * @property Simple|EtatDevis[]        $etatDevis
 *
 * @method   Simple|Module[]           getModule($parameters = null)
 * @method   Simple|Projet[]           getProjet($parameters = null)
 * @method   Simple|EtatDevis[]        getEtatDevis($parameters = null)
 *
 * @method   integer                   countModule()
 * @method   integer                   countProjet()
 * @method   integer                   countEtatDevis()
 */
class Devis extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var integer $id_projet */
    protected $id_projet;

    /** @var integer $id_etat */
    protected $id_etat;

    /** @var string $reference */
    protected $reference;

    /** @var string $date */
    protected $date;

    /** @var string $date_fin_validite */
    protected $date_fin_validite;

    /** @var string $note_publique */
    protected $note_publique;

    /** @var string $note_privee */
    protected $note_privee;

    /** @var double $remise */
    protected $remise;

    /** @var string $nom_client */
    protected $nom_client;

    /** @var string $prenom_client */
    protected $prenom_client;

    /** @var string $numero_rue_client */
    protected $numero_rue_client;

    /** @var string $rue_client */
    protected $rue_client;

    /** @var string $ville_client */
    protected $ville_client;

    /** @var string $cp_client */
    protected $cp_client;

    /** @var string $email_client */
    protected $email_client;

    /** @var string $tel_client */
    protected $tel_client;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Devis");

        $this->hasMany('id', 'App\Model\DevisFacture', 'id_devis',
            [
                'alias' => 'DevisFacture',
                'foreignKey' => true
            ]
        );

        $this->hasMany('id', 'App\Model\Module', 'id_devis',
            [
                'alias' => 'Module',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('id_projet', 'App\Model\Projet', 'id',
            [
                'alias' => 'Projet',
                'foreignKey' => [
                    'message' => "Le projet choisi pour ce devis n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_etat', 'App\Model\EtatDevis', 'id',
            [
                'alias' => 'EtatDevis',
                'foreignKey' => [
                    'message' => "L'état pour ce devis n'existe pas"
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function beforeValidationOnCreate()
    {
        $security = new Security\Random;

        $this->date = (new \DateTime)->format('Y-m-d H:i:s');
        $this->reference = "MDRD" . sprintf('%04d', time() . $security->number(16));

        /** @var Projet $projet */
        $projet = Projet::findFirst($this->id_projet);

        if ($projet !== false) {

            /** @var Client $client */
            $client = $projet->getClient();

            $this->nom_client = $client->getNom();
            $this->prenom_client = $client->getPrenom();
            $this->numero_rue_client = $client->getNumeroRue();
            $this->rue_client = $client->getRue();
            $this->tel_client = $client->getTel();
            $this->ville_client = $client->getVille();
            $this->cp_client = $client->getCp();
            $this->email_client = $client->getEmail();
        }
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Devis';
    }

    /**
     * Getter pour l'id du projet
     *
     * @return int
     */
    public function getIdProjet()
    {
        return (int) $this->id_projet;
    }

    /**
     * Setter pour l'id du projet
     *
     * @param int $id_projet
     */
    public function setIdProjet(int $id_projet)
    {
        $this->id_projet = $id_projet;
    }

    /**
     * Getter pour l'id de l'état du devis
     *
     * @return int
     */
    public function getIdEtat()
    {
        return (int) $this->id_etat;
    }

    /**
     * Setter pour l'id de l'état du devis
     *
     * @param int $id_etat
     */
    public function setIdEtat(int $id_etat)
    {
        $this->id_etat = $id_etat;
    }

    /**
     * Getter pour la référence
     *
     * @return string
     */
    public function getReference()
    {
        return "MDRD" . sprintf('%05d', $this->getId());
    }

    /**
     * Getter pour la date
     *
     * @return string
     */
    public function getDate()
    {
        return is_string($this->date)
            ? (new \DateTime($this->date))->format('d/m/Y') ?? null
            : null;
    }

    /**
     * Getter pour la date de la fin de validité
     *
     * @return string
     */
    public function getDateFinValidite()
    {
        return is_string($this->date_fin_validite)
            ? (new \DateTime($this->date_fin_validite))->format('d/m/Y') ?? null
            : null;
    }

    /**
     * Setter pour la date de la fin de validité
     *
     * @param string $date_fin_validite
     */
    public function setDateFinValidite(string $date_fin_validite)
    {
        $this->date_fin_validite = Helper::validateDate($date_fin_validite, 'd/m/Y')
            ? \DateTime::createFromFormat('d/m/Y', $date_fin_validite)->format('Y-m-d')
            : null;
    }

    /**
     * Getter pour la note publique
     *
     * @return string
     */
    public function getNotePublique()
    {
        return $this->note_publique;
    }

    /**
     * Setter pour la note publique
     *
     * @param string $note_publique
     */
    public function setNotePublique(string $note_publique)
    {
        $this->note_publique = $note_publique;
    }

    /**
     * Getter pour la note privée
     *
     * @return string
     */
    public function getNotePrivee()
    {
        return $this->note_privee;
    }

    /**
     * Setter pour la note privée
     *
     * @param string $note_privee
     */
    public function setNotePrivee(string $note_privee)
    {
        $this->note_privee = $note_privee;
    }

    /**
     * Getter pour la remise
     *
     * @return float
     */
    public function getRemise(): float
    {
        return (float) $this->remise;
    }

    /**
     * Setter pour la remise
     *
     * @param float $remise
     */
    public function setRemise(float $remise)
    {
        $this->remise = $remise;
    }

    /**
     * Getter pour le nom du client
     *
     * @return string
     */
    public function getNomClient()
    {
        return $this->nom_client;
    }

    /**
     * Getter pour le prénom du client
     *
     * @return string
     */
    public function getPrenomClient()
    {
        return $this->prenom_client;
    }

    /**
     * Getter pour le numéro de rue du client
     *
     * @return string
     */
    public function getNumeroRueClient()
    {
        return $this->numero_rue_client;
    }

    /**
     * Getter pour la rue du client
     *
     * @return string
     */
    public function getRueClient()
    {
        return $this->rue_client;
    }

    /**
     * Getter pour la ville du client
     *
     * @return string
     */
    public function getVilleClient()
    {
        return $this->ville_client;
    }

    /**
     * Getter pour le code postal du client
     *
     * @return string
     */
    public function getCpClient()
    {
        return $this->cp_client;
    }

    /**
     * Getter pour l'email du client
     *
     * @return string
     */
    public function getEmailClient()
    {
        return $this->email_client;
    }

    /**
     * Getter pour le tel du client
     *
     * @return string
     */
    public function getTelClient()
    {
        return $this->tel_client;
    }
}
