<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Helper;
use App\Library\Model\ModelBase;
use App\Model\Utilisateur;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class UtilisateurController
 * @package App\Controller
 *
 * @apiDefine DataUtilisateurApi
 * @apiParam {String} nom Nom de l'utilisateur
 * @apiParam {String} prenom Prénom de l'utilisateur
 * @apiParam {String} email Email de l'utilisateur
 * @apiParam {String} tel Téléphone de l'utilisateur
 * @apiParam {String} role Rôle de l'utilisateur
 * @apiParam {String} mdp Mot de passe de l'utilisateur
 * @apiParam {String} conf_mdp Confirmation du mot de passe de l'utilisateur
 */
class UtilisateurController extends ControllerApi
{
    /** @var string $model */
    protected $model = Utilisateur::class;

    /** @var array $columns */
    protected $columns = ['id', 'nom', 'prenom', 'email', 'tel', 'role'];

    /**
     * Méthode d'affichage du profil utilisateur
     *
     * @return array
     * @throws \Exception
     */
    public function profilAction()
    {
        $id_utilisateur = Helper::getTokenSession()->id_utilisateur;

        /** @var Utilisateur $utilisateur */
        $utilisateur = Utilisateur::findFirst($id_utilisateur);

        if ($utilisateur !== false) {

            if ($this->request->isPost()) {

                $utilisateur->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $utilisateur->getNom()));
                $utilisateur->setPrenom($this->request->getPost('prenom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $utilisateur->getPrenom()));
                $utilisateur->setTel($this->request->getPost('tel', Filter::FILTER_TRIM, $utilisateur->getTel()));

                if ($this->request->hasPost('mdp') && !empty($this->request->getPost('mdp'))) {

                    $mdp = $this->request->getPost('mdp', Filter::FILTER_TRIM);
                    $conf_mdp = $this->request->getPost('conf_mdp', Filter::FILTER_TRIM);

                    $utilisateur->setMdp($mdp, $conf_mdp);
                }

                if ($utilisateur->update() !== false) {

                    $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                    return [
                        'status' => 'SUCCESS',
                        'latency' => round((microtime(true) - $requestTime) * 1000),
                        'data' => $utilisateur->toArray($this->columns)
                    ];
                }

                $this->response->setStatusCode(409, 'Conflict');

                return [
                    'status' => 'FAILED',
                    'messages' => Helper::getMessages($utilisateur)
                ];

            } else {

                $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

                return [
                    'status' => 'SUCCESS',
                    'latency' => round((microtime(true) - $requestTime) * 1000),
                    'data' => $utilisateur->toArray($this->columns)
                ];
            }

        } else {

            $this->response->setStatusCode(404, 'Not Found');

            return [
                'status' => 'FAILED',
                'message' => "L'authentification à échoué, vous devez entrer un identifiant et un mot de passe."
            ];
        }
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->orWhere('email LIKE :email:', ['email' => '%' . $query . '%']);
        $criteria->orWhere('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
        $criteria->orWhere('prenom LIKE :prenom:', ['prenom' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Utilisateur $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setPrenom($this->request->getPost('prenom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getPrenom()));
        $model->setEmail($this->request->getPost('email', Filter::FILTER_EMAIL, $model->getEmail()));
        $model->setTel($this->request->getPost('tel', Filter::FILTER_TRIM, $model->getTel()));
        $model->setRole($this->request->getPost('role', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getRole()));

        if ($this->request->hasPost('mdp') && !empty($this->request->getPost('mdp'))) {

            $mdp = $this->request->getPost('mdp', Filter::FILTER_TRIM);
            $conf_mdp = $this->request->getPost('conf_mdp', Filter::FILTER_TRIM);

            $model->setMdp($mdp, $conf_mdp);
        }
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchUtilisateurApi
     * @apiGroup UtilisateurApi
     * @apiVersion 0.0.1
     * @api {post} /utilisateur/search Recherche des éléments
     * @apiPermission {role} | Utilisateur | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateUtilisateurApi
     * @apiGroup UtilisateurApi
     * @apiVersion 0.0.1
     * @api {post} /utilisateur Ajoute un nouvel élément
     * @apiPermission {role} | Utilisateur | create
     * @apiUse CreateGenericApi
     * @apiUse DataUtilisateurApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadUtilisateurApi
     * @apiGroup UtilisateurApi
     * @apiVersion 0.0.1
     * @api {get} /utilisateur/:id Affiche un élément
     * @apiPermission {role} | Utilisateur | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateUtilisateurApi
     * @apiGroup UtilisateurApi
     * @apiVersion 0.0.1
     * @api {post} /utilisateur/:id Modifie un élément
     * @apiPermission {role} | Utilisateur | update
     * @apiUse UpdateGenericApi
     * @apiUse DataUtilisateurApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteUtilisateurApi
     * @apiGroup UtilisateurApi
     * @apiVersion 0.0.1
     * @api {delete} /utilisateur/:id Supprime un élément
     * @apiPermission {role} | Utilisateur | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
