<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Builder\Project\Simple;

/**
 * Class Gamme
 * @package App\Model
 *
 * @property Simple|TypeGamme[]        $typeGamme
 * @property Simple|TypeFinition[]     $typeFinition
 * @property Simple|TypeIsolant[]      $typeIsolant
 *
 * @method   Simple|TypeGamme[]        getTypeGamme($parameters = null)
 * @method   Simple|TypeFinition[]     getTypeFinition($parameters = null)
 * @method   Simple|TypeIsolant[]      getTypeIsolant($parameters = null)
 *
 * @method   integer                   countTypeGamme()
 * @method   integer                   countTypeFinition()
 * @method   integer                   countTypeIsolant()
 */
class Gamme extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $qualite_huisserie */
    protected $qualite_huisserie;

    /** @var string $nom */
    protected $nom;

    /** @var integer $id_type_gamme */
    protected $id_type_gamme;

    /** @var integer $id_type_finition */
    protected $id_type_finition;

    /** @var integer $id_type_isolant */
    protected $id_type_isolant;

    /** @var string $description */
    protected $description;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Gamme");

        $this->hasMany('id', 'App\Model\Module', 'id_gamme',
            [
                'alias' => 'Module',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('id_type_gamme', 'App\Model\TypeGamme', 'id',
            [
                'alias' => 'TypeGamme',
                'foreignKey' => [
                    'message' => "Le type de gamme choisi n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_type_finition', 'App\Model\TypeFinition', 'id',
            [
                'alias' => 'TypeFinition',
                'foreignKey' => [
                    'message' => "Le type de finition choisie n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_type_isolant', 'App\Model\TypeIsolant', 'id',
            [
                'alias' => 'TypeIsolant',
                'foreignKey' => [
                    'message' => "Le type d'isolant choisi n'existe pas"
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Gamme';
    }

    /**
     * Getter pour la qualité des huisseries
     *
     * @return string
     */
    public function getQualiteHuisserie()
    {
        return $this->qualite_huisserie;
    }

    /**
     * Setter pour la qualité des huisseries
     *
     * @param string $qualite_huisserie
     */
    public function setQualiteHuisserie(string $qualite_huisserie)
    {
        $this->qualite_huisserie = $qualite_huisserie;
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Getter pour l'id de type de gamme
     *
     * @return int
     */
    public function getIdTypeGamme()
    {
        return (int) $this->id_type_gamme;
    }

    /**
     * Setter pour l'id de type de gamme
     *
     * @param int $id_type_gamme
     */
    public function setIdTypeGamme(int $id_type_gamme)
    {
        $this->id_type_gamme = $id_type_gamme;
    }

    /**
     * Getter pour l'id type finition
     *
     * @return int
     */
    public function getIdTypeFinition()
    {
        return (int) $this->id_type_finition;
    }

    /**
     * Setter pour l'id type finition
     *
     * @param int $id_type_finition
     */
    public function setIdTypeFinition(int $id_type_finition)
    {
        $this->id_type_finition = $id_type_finition;
    }

    /**
     * Getter pour l'id de type d'isolant
     *
     * @return int
     */
    public function getIdTypeIsolant()
    {
        return (int) $this->id_type_isolant;
    }

    /**
     * Setter pour l'id de type d'isolant
     *
     * @param int $id_type_isolant
     */
    public function setIdTypeIsolant(int $id_type_isolant)
    {
        $this->id_type_isolant = $id_type_isolant;
    }

    /**
     * Getter pour la dezscription
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter pour la description
     *
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
}
