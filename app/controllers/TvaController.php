<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\Tva;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class TvaController
 * @package App\Controller
 *
 * @apiDefine DataTvaApi
 * @apiParam {String} label Label de la Tva
 * @apiParam {Float} taux Taux de la Tva
 * @apiParam {Date} date_debut_validite Date de début de validité de la Tva
 * @apiParam {Date} date_fin_validite Date de fin de validité de la Tva
 */
class TvaController extends ControllerApi
{
    /** @var string $model */
    protected $model = Tva::class;

    /** @var array $columns */
    protected $columns = ['id', 'label', 'taux', 'date_debut_validite', 'date_fin_validite'];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('label LIKE :label:', ['label' => '%' . $query . '%']);
        $criteria->orWhere('taux LIKE :taux:', ['taux' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Tva $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setLabel($this->request->getPost('label', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getLabel()));
        $model->setTaux($this->request->getPost('taux', Filter::FILTER_FLOAT_CAST, $model->getTaux()));
        $model->setDateDebutValidite($this->request->getPost('date_debut_validite', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getDateDebutValidite()));
        $model->setDateFinValidite($this->request->getPost('date_fin_validite', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getDateFinValidite()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchTvaApi
     * @apiGroup TvaApi
     * @apiVersion 0.0.1
     * @api {post} /tva/search Recherche des éléments
     * @apiPermission {role} | Tva | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateTvaApi
     * @apiGroup TvaApi
     * @apiVersion 0.0.1
     * @api {post} /tva Ajoute un nouvel élément
     * @apiPermission {role} | Tva | create
     * @apiUse CreateGenericApi
     * @apiUse DataTvaApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadTvaApi
     * @apiGroup TvaApi
     * @apiVersion 0.0.1
     * @api {get} /tva/:id Affiche un élément
     * @apiPermission {role} | Tva | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateTvaApi
     * @apiGroup TvaApi
     * @apiVersion 0.0.1
     * @api {post} /tva/:id Modifie un élément
     * @apiPermission {role} | Tva | update
     * @apiUse UpdateGenericApi
     * @apiUse DataTvaApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteTvaApi
     * @apiGroup TvaApi
     * @apiVersion 0.0.1
     * @api {delete} /tva/:id Supprime un élément
     * @apiPermission {role} | Tva | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
