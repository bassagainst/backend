<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Task;

use App\Library\Model\ModelBase;
use App\Library\Task\TaskBase;

/**
 * Class SeedTask
 * @package App\Task
 */
class SeedTask extends TaskBase
{
    /**
     * Méthode de génération aléatoire d'un jeu de données
     * @throws \Exception
     */
    public function mainAction()
    {
        global $argv;

        // Efface les arguments nécessaires a l'accès de cette méthode pour la suite du script
        $argv = array_slice($argv, 2);

        $seedName = 'App\\Seeds\\' . ($argv[0] ?? null) . 'Seed';

        if (!isset($argv[1])) {

            self::nl("Vous devez spécifié la quantité de données généré !", 'red');
        }

        if (!isset($argv[0]) || !class_exists($seedName)) {

            self::nl("Vous devez spécifier un modèle de données dans la liste suivante : ", 'red');

            $elements = new \DirectoryIterator($this->di->get('config')->get('application')->get('seedsDir'));

            foreach ($elements as $el) {

                if ($el->isFile()) {

                    self::nl(' - ' . substr($el->getFilename(), 0, -8));
                }
            }
        }

        if (isset($argv[0]) && isset($argv[1]) && class_exists($seedName)) {

            self::nl("Création de l'élément <" . $argv[0] . "> en " . $argv[1] . " exemplaires" . PHP_EOL);

            for ($i = 0; $i < $argv[1]; $i++) {

                /** @var ModelBase $seedObject */
                $seedObject = new $seedName;

                $seedObject->save()
                    ? self::nl('.', 'green', null, false)
                    : self::nl('!', 'red', null, false);

                $modelsCache = $this->getDI()->get('modelsCache');
                $modelsCache->flush();
            }

            self::nl(PHP_EOL . PHP_EOL . "Fin de la génération des données", 'green');

            exit(255);
        }

        self::nl(PHP_EOL . 'Exemple : ', 'yellow');
        self::nl(" -> ./console seed [Utilisateur, ...] [0-9]", 'yellow');
    }
}
