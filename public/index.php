<?php
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

use App\Library\Plugin\PreFlightPlugin;
use Phalcon\Http\Request;
use Phalcon\Mvc\Application;

define('ROOTPATH', dirname(__DIR__));
define('STARTTIME', microtime(true));

try {

    // Importation du bootstrap
    require_once ROOTPATH . '/app/Bootstrap.php';

    // Déclaration du bootstrap
    $bootstrap = new Bootstrap;

    // Injection des dépendances
    $bootstrap->load();

    // Importation des services Mvc
    $services = new Phalcon\Config(
        [
            \App\Library\Provider\MvcProvider::class
        ]
    );

    // Démarrage de l'application
    $app = new Application($bootstrap->register($services));
    $app->useImplicitView(false);
    $app->handle()->send();

} catch (\Throwable $e) {
    
    $response = new \Phalcon\Http\Response;
    PreFlightPlugin::fillHeaders($response, (new Request));
    $response->setStatusCode(500, 'Internal Server Error');

    $result = [
        'status' => 'FATALERROR',
        'message' => "Le serveur à subi une erreur fatale, veuillez contacter votre administrateur système !"
    ];

    $config = new Phalcon\Config\Adapter\Php(ROOTPATH . '/app/config/config.php');

    // Affiche les informations de l'exception si le mode de fonctionnement est <dev>
    if (isset($config) && $config->get('application')->offsetExists('mode')
        && $config->get('application')->get('mode') === 'dev') {

        $result += [
            'mode'   => 'dev',
            'errors' => [
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
                'trace'   => $e->getTrace()
            ]
        ];
    }

    $response->setJsonContent($result);
    $response->send();
}
