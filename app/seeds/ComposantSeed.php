<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Seeds;

use App\Library\Model\SeedsTrait;
use App\Model\Composant;

/**
 * Class ComposantSeed
 * @package App\Model
 */
class ComposantSeed extends Composant
{
    use SeedsTrait;

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function onConstruct()
    {
        $uniteColisage = ['kg', 'pc', 't', 'g', 'm3'];

        $this->setNom(self::faker()->countryCode . self::faker()->countryISOAlpha3 . self::faker()->countryCode);
        $this->setPrixHt(self::faker()->randomFloat(2, 0.5, 2000));
        $this->setMarge(self::faker()->randomFloat(2, 0.05, 0.35));
        $this->setColisage(intval(rand(1, 10) . '0'));
        $this->setDescription(self::faker()->text(1000));
        $this->setUniteColisage($uniteColisage[rand(0, count($uniteColisage) - 1)]);

        /** @noinspection PhpUndefinedMethodInspection */
        $this->setIdTva($this->generateTva());

        /** @noinspection PhpUndefinedMethodInspection */
        $this->setIdFournisseur($this->generateFournisseur());

        /** @noinspection PhpUndefinedMethodInspection */
        $this->setIdFamilleComposant($this->generateFamilleComposant());
    }
}
