<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\Fournisseur;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class FournisseurController
 * @package App\Controller
 *
 * @apiDefine DataFournisseurApi
 * @apiParam {String} nom Nom du fournisseur
 * @apiParam {String} numero_rue Numéro de rue du fournisseur
 * @apiParam {String} rue Rue du fournisseur
 * @apiParam {String} ville Ville du fournisseur
 * @apiParam {String} cp Code postal du fournisseur
 * @apiParam {String} tel Téléphone du fournisseur
 */
class FournisseurController extends ControllerApi
{
    /** @var string $model */
    protected $model = Fournisseur::class;

    /** @var array $columns */
    protected $columns = ['id', 'nom', 'numero_rue', 'rue', 'ville', 'cp', 'tel'];

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('nom LIKE :nom:', ['nom' => '%' . $query . '%']);
        $criteria->orWhere('ville LIKE :ville:', ['ville' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|Fournisseur $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setNom($this->request->getPost('nom', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getNom()));
        $model->setNumeroRue($this->request->getPost('numero_rue', Filter::FILTER_TRIM, $model->getNumeroRue()));
        $model->setRue($this->request->getPost('rue', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getRue()));
        $model->setVille($this->request->getPost('ville', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getVille()));
        $model->setCp($this->request->getPost('cp', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getCp()));
        $model->setTel($this->request->getPost('tel', Filter::FILTER_TRIM, $model->getTel()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchFournisseurApi
     * @apiGroup FournisseurApi
     * @apiVersion 0.0.1
     * @api {post} /fournisseur/search Recherche des éléments
     * @apiPermission {role} | Fournisseur | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateFournisseurApi
     * @apiGroup FournisseurApi
     * @apiVersion 0.0.1
     * @api {post} /fournisseur Ajoute un nouvel élément
     * @apiPermission {role} | Fournisseur | create
     * @apiUse CreateGenericApi
     * @apiUse DataFournisseurApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadFournisseurApi
     * @apiGroup FournisseurApi
     * @apiVersion 0.0.1
     * @api {get} /fournisseur/:id Affiche un élément
     * @apiPermission {role} | Fournisseur | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateFournisseurApi
     * @apiGroup FournisseurApi
     * @apiVersion 0.0.1
     * @api {post} /fournisseur/:id Modifie un élément
     * @apiPermission {role} | Fournisseur | update
     * @apiUse UpdateGenericApi
     * @apiUse DataFournisseurApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteFournisseurApi
     * @apiGroup FournisseurApi
     * @apiVersion 0.0.1
     * @api {delete} /fournisseur/:id Supprime un élément
     * @apiPermission {role} | Fournisseur | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
