<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

use Phalcon\Config\Adapter\Php as ConfigAdapter;
use Phalcon\Di;
use Phalcon\Loader;

/**
 * Class Bootstrap
 *
 * Le bootstrap permet d'unifier le chargement des dépendances et du Di au sein de l'application
 */
class Bootstrap
{

    /** @var Di $di */
    protected $di;

    /**
     * Bootstrap constructor.
     *
     * @param string $configPath
     */
    public function __construct(string $configPath = 'config.php')
    {
        // Importe l'injecteur de dépendances
        $this->di = new Di;

        // Importation de la configuration
        $this->di->setShared('config', function () use ($configPath) {

            // Charge le fichier de configuration
            $config = new ConfigAdapter(ROOTPATH . '/app/config/' . $configPath);

            if (!$config->offsetExists('database')
                || !$config->offsetExists('security')
                || !$config->offsetExists('application')) {

                $message = "Erreur de configuration (core) est survenue dans database, security ou application";

                error_log($message);
                throw new \Exception($message);
            }

            /** @var \Phalcon\Config $security */
            $security = $config->get('security');

            if (!$security->offsetExists('jwtKey')
                || !$security->offsetExists('jwtType')
                || !$security->offsetExists('jwtExpire')
                || !$security->offsetExists('jwtRenew')
                || !$security->offsetExists('jwtRefresh')
                || (!$security->offsetExists('jwtAlg')
                    && !is_array($security->get('jwtAlg')))) {

                $message = "Erreur de configuration (security) est survenue lors de la configuration de JWT";

                error_log($message);
                throw new \Exception($message);
            }

            /** @var \Phalcon\Config $application */
            $application = $config->get('application');

            if (!$application->offsetExists('controllersDir')
                || !$application->offsetExists('libraryDir')
                || !$application->offsetExists('modelsDir')
                || !$application->offsetExists('migrationsDir')) {

                $message = "Erreur de configuration (application) est survenue lors de la configuration du loader";

                error_log($message);
                throw new \Exception($message);
            }

            return $config;
        });

        // Définition de la zone de temps par défaut
        date_default_timezone_set($this->di->getShared('config')->application->timezone ?? 'Europe/Paris');

        return $this;
    }

    /**
     * Retourne l'injecteur de dépendances de l'application
     *
     * @return Di
     */
    public function getDi()
    {
        return $this->di;
    }

    /**
     * Méthode de chargement des dépendances
     *
     * @param array $registerNs
     * @param array $registerDirs
     * @return $this
     */
    public function load(array $registerNs = [], array $registerDirs = [])
    {
        $registerNamespaces = [
            "App\\Controller" => $this->di->getShared('config')->application->controllersDir,
            "App\\Library" => $this->di->getShared('config')->application->libraryDir,
            "App\\Model" => $this->di->getShared('config')->application->modelsDir
        ];

        // Enregistre les classes Php et Composer
        $loader = new Loader();
        $loader->registerNamespaces(
            array_merge($registerNamespaces, $registerNs)
        )->registerFiles(
            [
                ROOTPATH . '/vendor/autoload.php'
            ]
        );

        if (!empty($registerDirs)) {

            $loader->registerDirs($registerDirs);
        }

        $loader->register();

        return $this;
    }

    /**
     * Enregistrement final du Di
     *
     * @param null|\Phalcon\Config $servicesExt
     * @return Di
     * @throws Exception
     */
    public function register($servicesExt = null)
    {
        $services = new ConfigAdapter(ROOTPATH . '/app/config/services.php');

        if (isset($servicesExt) && $servicesExt instanceof \Phalcon\Config) {

            $services->merge($servicesExt);
        }

        // Enregistre les différents services
        $provider = new \App\Library\Provider($this->di);
        $provider->load($services)->register();

        return $this->di;
    }
}
