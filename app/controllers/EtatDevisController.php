<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerApi;
use App\Library\Model\ModelBase;
use App\Model\EtatDevis;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Criteria;

/**
 * Class EtatDevisController
 * @package App\Controller
 *
 * @apiDefine DataEtatDevisApi
 * @apiParam {String} label Label de l'état du devis
 * @apiParam {Number} ordre Ordre de l'état du devis
 */
class EtatDevisController extends ControllerApi
{
    /** @var string $model */
    protected $model = EtatDevis::class;

    /** @var array $columns */
    protected $columns = ['id', 'label', 'ordre'];

    /**
     * @param Criteria $criteria
     */
    public function filters(Criteria $criteria)
    {
        $criteria->orderBy('ordre');
    }

    /**
     * @inheritdoc
     *
     * @param Criteria $criteria
     */
    protected function atSearch(Criteria $criteria)
    {
        $query = $this->request->getPost('query', Filter::FILTER_STRIPTAGS);
        $criteria->where('label LIKE :label:', ['label' => '%' . $query . '%']);
    }

    /**
     * @inheritdoc
     *
     * @param ModelBase|EtatDevis $model
     */
    protected function beforeCreateUpdate(ModelBase $model)
    {
        $model->setLabel($this->request->getPost('label', [Filter::FILTER_STRIPTAGS, Filter::FILTER_TRIM], $model->getLabel()));
        $model->setOrdre($this->request->getPost('ordre', Filter::FILTER_ABSINT, $model->getOrdre()));
    }

    /**
     * @inheritdoc
     *
     * @apiName SearchEtatDevisApi
     * @apiGroup EtatDevisApi
     * @apiVersion 0.0.1
     * @api {post} /etat-devis/search Recherche des éléments
     * @apiPermission {role} | EtatDevis | search
     * @apiUse SearchGenericApi
     */
    public function searchAction(): array
    {
        return parent::searchAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName CreateEtatDevisApi
     * @apiGroup EtatDevisApi
     * @apiVersion 0.0.1
     * @api {post} /etat-devis Ajoute un nouvel élément
     * @apiPermission {role} | EtatDevis | create
     * @apiUse CreateGenericApi
     * @apiUse DataEtatDevisApi
     */
    public function createAction(): array
    {
        return parent::createAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName ReadEtatDevisApi
     * @apiGroup EtatDevisApi
     * @apiVersion 0.0.1
     * @api {get} /etat-devis/:id Affiche un élément
     * @apiPermission {role} | EtatDevis | read
     * @apiUse ReadGenericApi
     */
    public function readAction(): array
    {
        return parent::readAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName UpdateEtatDevisApi
     * @apiGroup EtatDevisApi
     * @apiVersion 0.0.1
     * @api {post} /etat-devis/:id Modifie un élément
     * @apiPermission {role} | EtatDevis | update
     * @apiUse UpdateGenericApi
     * @apiUse DataEtatDevisApi
     */
    public function updateAction(): array
    {
        return parent::updateAction();
    }

    /**
     * @inheritdoc
     *
     * @apiName DeleteEtatDevisApi
     * @apiGroup EtatDevisApi
     * @apiVersion 0.0.1
     * @api {delete} /etat-devis/:id Supprime un élément
     * @apiPermission {role} | EtatDevis | delete
     * @apiUse DeleteGenericApi
     */
    public function deleteAction(): array
    {
        return parent::deleteAction();
    }
}
