<?php
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

use Phalcon\Di;

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOTPATH', dirname(__DIR__));

try {

    // Importation du bootstrap
    require_once ROOTPATH . '/app/Bootstrap.php';

    // Instanciation du Bootstrap
    $bootstrap = new Bootstrap('config.test.php');

    // Importation des dépendances
    $bootstrap->load(
        [
            "App\\Seeds" => $bootstrap->getDi()->get('config')->get('application')->seedsDir
        ],
        [
            __DIR__
        ]
    );

    /** @var \Phalcon\DiInterface $di */
    $di = $bootstrap->register();

    /** @var \Phalcon\Db\AdapterInterface $db */
    $db = $di->get('db');

    // Vidage des tables entre deux tests
    foreach ($db->listTables() as $tables) {

        $db->delete($tables);
    }

    // Reset de l'injecteur de dépendances
    Di::reset();

} catch (\Throwable $e) {

    // Affiche l'erreur récupérer
    echo $e->getTraceAsString();
}
