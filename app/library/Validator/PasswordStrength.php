<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Validator;

use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;

/**
 * Class PasswordStrength
 * @package App\Library\Validator
 */
class PasswordStrength extends Validator
{

    const MIN_VALID_SCORE = 2;

    /**
     * Méthode de validation d'un mot de passe
     *
     * @inheritdoc
     *
     * @param \Phalcon\Validation $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(\Phalcon\Validation $validation, $attribute)
    {
        $allowEmpty = $this->getOption('allowEmpty');
        $password = $validation->getValue($attribute);

        // Vérifie si le mot de passe peu être vide, vérifie ensuite si le mot de passe est scalaire ou null
        if ($allowEmpty && ((is_scalar($password) && (string) $password === '') || is_null($password))) {

            return true;
        }

        $minScore = ($this->hasOption('minScore') ? $this->getOption('minScore') : self::MIN_VALID_SCORE);

        // Vérifie si le score du mot de passe est supérireur ou égal au score minimum
        $scorePassword = $this->scoreCalculator($password);

        // Vérifie si le mot de passe est une chaine de caractères,
        // ensuite, vérification vérifie le score calculé précédement
        if (is_string($password) && $scorePassword >= $minScore) {

            return true;
        }

        $message = ($this->hasOption('message')
            ? $this->getOption('message') . " (score : $scorePassword / $minScore)"
            : "le mot de passe ne respecte pas les consignes de sécurité (score : $scorePassword / $minScore)");

        $validation->appendMessage(new Message($message, $attribute, 'PasswordStrengthValidator'));

        return false;
    }

    /**
     * Méthode de calcul du score
     *
     * @param $password
     * @return int
     */
    private function scoreCalculator($password)
    {
        $scoreChecker = 0;
        $hasLower = preg_match('![a-z]!', $password);
        $hasUpper = preg_match('![A-Z]!', $password);
        $hasNumber = preg_match('![0-9]!', $password);

        // Augmente le score si le mot de passe utilise des majuscules et des minuscules
        if ($hasLower && $hasUpper) {

            ++$scoreChecker;
        }

        // Augmente le score si le mot de passe utilise majuscule ou minuscule avec des nombres
        if (($hasNumber && $hasLower) || ($hasNumber && $hasUpper)) {

            ++$scoreChecker;
        }

        // Augmente le score si le mot de passe utilise nombres, majuscules et minuscules
        if (preg_match('![^0-9a-zA-Z]!', $password)) {

            ++$scoreChecker;
        }

        // Le max score à cette étape est 3 (mot de passe avec nombres, majuscules, minuscules)

        $passwordLength = mb_strlen($password);

        // Si le mot de passe dépasse la longueur de 16 ajoute 2 au score,
        // sinon si il dépasse 8 ajoute 1 au score,
        // sinon si il fait entre 2 et 4 caractère, diminue le score de 1,
        // sinon si le score précédent est égal a 0 et qu'il dépasse 0 caractères, ajoute 1 au score
        if ($passwordLength >= 16) {

            $scoreChecker += 2;

        } elseif ($passwordLength >= 8) {

            ++$scoreChecker;

        } elseif ($passwordLength <= 4 && $scoreChecker > 1) {

            --$scoreChecker;

        } elseif ($passwordLength > 0 && $scoreChecker === 0) {

            ++$scoreChecker;
        }

        return $scoreChecker;
    }
}
