<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Provider;

use Phalcon\Db\Adapter\Pdo\Factory as Database;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Model\Manager as ModelManager;
use Phalcon\Mvc\Model\Metadata\Redis as MetaDataAdapter;

/**
 * Class DatabaseProvider
 * @package App\Library\Provider
 */
class DatabaseProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     *
     * @param \Phalcon\DiInterface $di
     */
    public function register(\Phalcon\DiInterface $di)
    {
        /*
         |--------------------------------------------------------------------------
         | Service de base de données
         |--------------------------------------------------------------------------
         | Ce service permet à l'application de d'accéder à la base de données.
         |
         */
        $di->setShared('db', function () use($di) {

            /** @var \Phalcon\Config $params */
            $params = $di->getShared('config')->get('database');

            if (!$params->offsetExists('adapter'))
                throw new \Exception("Erreur de configuration (database), veuillez contacter votre administrateur système");

            if (!$params->offsetExists('charset'))
                $params->offsetSet('charset', 'utf8');

            return Database::load($params);
        });

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des métadatas
         |--------------------------------------------------------------------------
         | Ce service permet à l'application de mettre en cache les méta données.
         |
         */
        $di->setShared('modelsManager', function () {
            return new ModelManager();
        });

        /*
         |--------------------------------------------------------------------------
         | Service de gestion des métadatas
         |--------------------------------------------------------------------------
         | Ce service permet à l'application de mettre en cache les méta données.
         |
         */
        $di->setShared('modelsMetadata', function () use ($di) {

            /** @var \Phalcon\Config $params */
            $params = $di->getShared('config')->get('redis');

            return new MetaDataAdapter(
                [
                    "host"       => $params->get('host'),
                    "port"       => $params->get('port'),
                    "persistent" => 0,
                    "statsKey"   => "_PHCM_MM",
                    "lifetime"   => 172800,
                    "index"      => $params->get('index') + 1,
                ]
            );
        });

        $di->set('modelsCache', function () use ($di) {

            /** @var \Phalcon\Config $params */
            $params = $di->getShared('config')->get('redis');

            $frontCache = new \Phalcon\Cache\Frontend\Data(
                [
                    'lifetime' => 86400,
                ]
            );

            $cache = new \Phalcon\Cache\Backend\Redis(
                $frontCache,
                [
                    'host'       => $params->get('host'),
                    'port'       => $params->get('port'),
                    "persistent" => 0,
                    "statsKey"   => "_PHCM_MC",
                    'index'      => $params->get('index') + 2,
                ]
            );

            return $cache;

        });
    }
}
