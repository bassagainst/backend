<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

use Faker\Factory as FakerFactory;
use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;

/**
 * Class UnitTestCase
 *
 * Base commune pour tout les tests unitaires
 */
abstract class UnitTestCase extends PhalconTestCase
{
    const RAND_GENERATION = 20;

    /** @var Di $di */
    protected $di;

    /** @var  \Faker\Generator $faker */
    protected $faker;

    /**
     * Méthode déclanchée avant chaques tests unitaires
     *
     * @throws Exception
     */
    public function setUp()
    {
        // Déclanche les mécaniques du parent
        parent::setUp();

        // Importe le Bootstrap
        $bootstrap = new Bootstrap('config.test.php');

        $di = $bootstrap->register();

        /** @var \Phalcon\Acl\Adapter\Memory $acl */
        $acl = $di->get('acl');

        $acl->addRole('Administrateur');

        $this->di = $di;

        // Suppression du modelsCache
        !$di->has('modelsCache') ?: $di->remove('modelsCache');

        $this->faker = FakerFactory::create();

        Di::setDefault($di);
    }

    /**
     * Méthode de simplification d'envoi des messages au STDOUT
     *
     * @param $class
     * @param $text
     */
    public static function message($class, $text)
    {
        try {

            echo "-> " . (new ReflectionClass($class))->getName() . ' : ' . $text . PHP_EOL;

        } catch (ReflectionException $e) {

            echo 'ERROR: ' . $e->getTraceAsString();
            exit(255);
        }
    }
}
