<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerBase;
use App\Library\Helper;
use App\Model\Utilisateur;
use Faker\Factory as FakerFactory;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Microsoft\Graph\Graph;
use Phalcon\Filter;

/**
 * Class AuthController
 * @package App\Controller
 *
 * @apiDefine CommonSuccessErrorAuthApi
 * @apiSuccess {String} status Affiche le status du résultat
 * @apiSuccess {Object} data Affiche les tokens de connection utilisateur
 * @apiSuccess {String} data.token Token courte durée de requêtage des pages
 * @apiSuccess {String} data.refreshToken Token longue durée de renouvellement du token
 */
class AuthController extends ControllerBase
{
    /**
     * Méthode de connexion à l'API
     *
     * @return array
     * @throws \Exception
     *
     * @apiName LoginAuthApi
     * @apiGroup AuthApi
     * @apiVersion 0.0.1
     * @api {post} /auth/login Connecte l'utilisateur
     * @apiParam {String} email Adresse email de l'utilisateur
     * @apiParam {String} mdp Mot de passe de l'utilisateur
     * @apiUse CommonSuccessErrorAuthApi
     *
     * @apiError NotFound L'utilisateur et/ou le mot de passe est incorrect
     */
    public function loginAction()
    {
        $email = $this->request->getPost('email', Filter::FILTER_EMAIL);
        $mdp = $this->request->getPost('mdp', Filter::FILTER_STRING);

        if (empty($email) && empty($mdp)) {

            $this->response->setStatusCode(404, 'Not Found');

            return [
                'status' => 'FAILED',
                'message' => "L'authentification à échoué, vous devez entrer un identifiant et un mot de passe."
            ];
        }

        $utilisateur = Utilisateur::findFirst(
            [
                'conditions' => 'email = ?1',
                'bind' => [
                    1 => $email
                ]
            ]
        );

        if ($utilisateur !== false
            && $utilisateur instanceof Utilisateur
            && $utilisateur->checkMdp($mdp)) {

            return [
                'status' => 'SUCCESS',
                'data' => [
                    'token' => $utilisateur->getPayload(),
                    'refreshToken' => $utilisateur->getRefreshPayload()
                ]
            ];
        }

        // Protection contre les attaques de temps, ralenti l'exécution
        // du script quand l'utilisateur ou le mot de passe est éronné
        $this->security->hash(rand());

        $this->response->setStatusCode(418, 'I\'m a teapot');

        return [
            'status' => 'FAILED',
            'message' => "L'authentification à échoué, l'identifiant ou le mot de passe est incorrect."
        ];
    }

    /**
     * Méthode de connection Sso avec Microsoft
     *
     * @return array
     * @throws IdentityProviderException
     * @throws \Microsoft\Graph\Exception\GraphException
     * @throws \Exception
     *
     * @apiName MicrosoftAuthApi
     * @apiGroup AuthApi
     * @apiVersion 0.0.1
     * @api {post} /auth/microsoft Connection Sso Microsoft
     * @apiHeader {String} redirectUri Url de redirection du Sso de Microsoft
     * @apiUse CommonSuccessErrorAuthApi
     *
     * @apiSuccess {String} api Retourne le lien d'api si le redirectUri est vide
     * @apiError Forbidden L'utilisateur n'est pas autorisé à rejoindre l'application
     */
    public function microsoftAction()
    {
        $redirectUri = $this->request->has('redirectUri')
            ? $this->request->get('redirectUri', Filter::FILTER_URL)
            : $this->request->getHeader('Origin');

        // https://apps.dev.microsoft.com/

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->getDI()->get('config')>get('sso')->get('clientId'),
            'clientSecret'            => $this->getDI()->get('config')>get('sso')->get('clientSecret'),
            'redirectUri'             => $redirectUri,
            'urlAuthorize'            => 'https://login.microsoftonline.com/common' . '/oauth2/v2.0/authorize',
            'urlAccessToken'          => 'https://login.microsoftonline.com/common' . '/oauth2/v2.0/token',
            'scopes'                  => $this->getDI()->get('config')>get('sso')->get('scopes'),
            'urlResourceOwnerDetails' => ''
        ]);

        if ($this->request->hasQuery('code')) {

            $code = $this->request->getQuery('code');

            $accessToken = $provider->getAccessToken('authorization_code', ['code' => $code]);

            $graph = new Graph;
            $graph->setAccessToken($accessToken->getToken());

            /** @var \Microsoft\Graph\Http\GraphResponse $request */
            $request = $graph->createRequest('GET', '/me')->execute();

            $user = $request->getBody();

            /** @var Utilisateur $utilisateur */
            $utilisateur = Utilisateur::findFirst(
                [
                    'conditions' => 'email = ?1 AND deleted LIKE 0',
                    'bind' => [
                        1 => $user['mail']
                    ]
                ]
            );

            if ($utilisateur === false) {

                unset($utilisateur);

                $utilisateur = Utilisateur::findFirst(
                    [
                        'conditions' => 'email = ?1 AND deleted LIKE 1',
                        'bind' => [
                            1 => $user['mail']
                        ]
                    ]
                );

                if ($utilisateur === false) {
                    $mdp = bin2hex(openssl_random_pseudo_bytes(8));

                    $utilisateur = new Utilisateur;
                    $utilisateur->setNom($user['surname'] ?? 'Anonymous');
                    $utilisateur->setPrenom($user['givenName'] ?? 'Anonymous');
                    $utilisateur->setEmail($user['mail']);
                    $utilisateur->setTel($user['mobilePhone'] ?? '0000000000');
                    $utilisateur->setRole('Utilisateur');
                    $utilisateur->setMdp($mdp, $mdp);

                } else {

                    $utilisateur->setDeleted(false);
                }

                $needle = false;

                foreach (['cesi.fr'] as $wl) {

                    if (strpos($user['mail'], $wl) !== false) {

                        $needle = true;
                        break;
                    }
                }

                if ($needle === false || $utilisateur->save() === false) {

                    $this->response->setStatusCode(400, 'Bad Request');

                    return [
                        'status' => 'ERROR',
                        'message' => "Vous ne pouvez pas vous connecter, veuillez contacter votre administrateur"
                    ];
                }
            }

            return [
                'status' => 'SUCCESS',
                'data' => [
                    'token' => $utilisateur->getPayload(),
                    'refreshToken' => $utilisateur->getRefreshPayload()
                ]
            ];

        } else {

            return [
                'status' => 'SUCCESS',
                'api' => $provider->getAuthorizationUrl()
            ];
        }
    }

    /**
     * Méthode de régénération d'un token d'authentification
     *
     * Doit reçevoir en POST le <refreshToken> afin de pouvoir régénérer.
     *
     * @return array
     * @throws \Exception
     *
     * @apiName RegenerateAuthApi
     * @apiGroup AuthApi
     * @apiVersion 0.0.1
     * @api {post} /auth/regenerate Régénération du token
     * @apiParam {String} refreshToken Token longue durée de rafraichissement
     * @apiUse CommonSuccessErrorAuthApi
     *
     * @apiSuccess {Boolean} data.refresh Booléen de retour du rafraichissement du token de longue durée
     * @apiError NotFound Le token de rafraichissement est invalide
     */
    public function regenerateAction()
    {
        $refreshToken = $this->request->getPost('refreshToken', Filter::FILTER_STRING);

        if (is_null($refreshToken)) {

            return $this->disconnectErrorEmptyMessage();
        }

        $payload = Utilisateur::refreshPayload($refreshToken);

        if ($payload !== false) {

            return [
                'status' => 'SUCCESS',
                'data' => [
                    // Envoie le nouveau token d'authentification
                    'token' => $payload,

                    // Retourne le token de rafraichissement utilisé ou le nouveau token
                    'refreshToken' => $refreshToken,

                    // Retourne si le token de rafraichissement à été mis à jour
                    'refresh' => $this->request->getPost('refreshToken', Filter::FILTER_STRING) !== $refreshToken
                ]
            ];
        }

        return $this->disconnectErrorInvalidMessage();
    }

    /**
     * Méthode de déconnection de l'API
     *
     * @return array
     *
     * @apiName RegenerateAuthApi
     * @apiGroup AuthApi
     * @apiVersion 0.0.1
     * @api {post} /auth/logout Suppression du token
     * @apiParam {String} refreshToken Token longue durée de rafraichissement pour suppression
     *
     * @apiSuccess {String} status Affiche le status du résultat
     * @apiSuccess {String} message Affiche le message de déconnection
     * @apiError NotFound Le token de rafraichissement est invalide
     */
    public function logoutAction()
    {
        $refreshToken = $this->request->getPost('refreshToken', Filter::FILTER_STRING);

        if (is_null($refreshToken)) {

            return $this->disconnectErrorEmptyMessage();
        }

        /** @var \Redis $redis */
        $redis = $this->di->get('redis');

        try {

            $data = Helper::decodeJwt($refreshToken);

        } catch (\UnexpectedValueException $e) {

            return $this->disconnectSuccessMessage();

        } catch (\Exception $e) {

            return $this->disconnectSuccessMessage();
        }

        if ($redis->del($data->payload->token)) {

            return $this->disconnectSuccessMessage();
        }

        return $this->disconnectErrorInvalidMessage();
    }

    /**
     * Message retourné en cas de déconnexion token vide
     *
     * @return array
     */
    private function disconnectErrorEmptyMessage()
    {
        $this->response->setStatusCode(404, 'Not Found');

        return [
            'status' => 'FAILED',
            'message' => "Le jeton de rafraichissement n'est pas défini !"
        ];
    }

    /**
     * Message retourné en cas de déconnexion ratée
     *
     * @return array
     */
    private function disconnectErrorInvalidMessage()
    {
        $this->response->setStatusCode(404, 'Not Found');

        return [
            'status' => 'FAILED',
            'message' => "Le jeton de rafraichissement est invalide !"
        ];
    }

    /**
     * Message retourné en cas de déconnexion validée
     *
     * @return array
     */
    private function disconnectSuccessMessage()
    {
        return [
            'status' => 'SUCCESS',
            'message' => "Vous êtes maintenant déconnecté"
        ];
    }
}
