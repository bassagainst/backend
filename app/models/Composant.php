<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * Class Composant
 * @package App\Model
 *
 * @property Simple|Fournisseur[]      $fournisseur
 * @property Simple|FamilleComposant[] $familleComposant
 * @property Simple|Tva[]              $tva
 *
 * @method   Simple|Fournisseur[]      getFournisseur($parameters = null)
 * @method   Simple|FamilleComposant[] getFamilleComposant($parameters = null)
 * @method   Simple|Tva[]              getTva($parameters = null)
 *
 * @method   integer                   countFournisseur()
 * @method   integer                   countFamilleComposant()
 * @method   integer                   countTva()
 */
class Composant extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var integer $id_fournisseur */
    protected $id_fournisseur;

    /** @var integer $id_famille_composant */
    protected $id_famille_composant;

    /** @var integer $colisage */
    protected $colisage;

    /** @var string $nom */
    protected $nom;

    /** @var double $prix_ht */
    protected $prix_ht;

    /** @var double $marge */
    protected $marge;

    /** @var string $description */
    protected $description;

    /** @var string $unite_colisage */
    protected $unite_colisage;

    /** @var integer $id_tva */
    protected $id_tva;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Composant");

        $this->hasMany('id', LiaisonModuleComposant::class, 'id_composant',
            [
                'alias'      => 'LiaisonModuleComposant',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('id_fournisseur', Fournisseur::class, 'id',
            [
                'alias'      => 'Fournisseur',
                'foreignKey' => [
                    'message' => "Le fournisseur choisi pour ce composant n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_tva', Tva::class, 'id',
            [
                'alias'      => 'Tva',
                'foreignKey' => [
                    'message' => "La TVA choisie pour ce composant n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_famille_composant', FamilleComposant::class, 'id',
            [
                'alias'      => 'FamilleComposant',
                'foreignKey' => [
                    'message' => "La famille de composant choisie pour ce composant n'existe pas"
                ]
            ]
        );

        $this->hasManyToMany(
            "id",
            "App\Model\LiaisonModuleComposant",
            "id_composant", "id_module",
            "App\Model\Module",
            "id",
            [
                'alias' => 'LiaisonModuleComposant'
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Composant';
    }

    /**
     * Getter pour l'id du fournisseur
     *
     * @return int
     */
    public function getIdFournisseur()
    {
        return (int) $this->id_fournisseur;
    }

    /**
     * Setter pour l'id du fournisseur
     *
     * @param int $id_fournisseur
     */
    public function setIdFournisseur(int $id_fournisseur)
    {
        $this->id_fournisseur = $id_fournisseur;
    }

    /**
     * Getter pour l'id de famille de composant
     * @return int
     */
    public function getIdFamilleComposant()
    {
        return (int) $this->id_famille_composant;
    }

    /**
     * Setter pour l'id de famille de composant
     *
     * @param int $id_famille_composant
     */
    public function setIdFamilleComposant(int $id_famille_composant)
    {
        $this->id_famille_composant = $id_famille_composant;
    }

    /**
     * Getter pour le colisage
     *
     * @return int
     */
    public function getColisage()
    {
        return (int) $this->colisage;
    }

    /**
     * Setter pour le colisage
     *
     * @param int $colisage
     */
    public function setColisage(int $colisage)
    {
        $this->colisage = $colisage;
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Getter pour le prix hors taxes
     *
     * @return float
     */
    public function getPrixHt()
    {
        return (float) $this->prix_ht;
    }

    /**
     * Setter pour le prix hors taxes
     *
     * @param float $prix_ht
     */
    public function setPrixHt(float $prix_ht)
    {
        $this->prix_ht = $prix_ht;
    }

    /**
     * Getter pour la marge
     *
     * @return float
     */
    public function getMarge()
    {
        return (float) $this->marge;
    }

    /**
     * Setter pour la marge
     *
     * @param float $marge
     */
    public function setMarge(float $marge)
    {
        $this->marge = $marge;
    }

    /**
     * Getter pour la description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter pour la description
     *
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * Getter pour l'unité de colisage
     *
     * @return string
     */
    public function getUniteColisage()
    {
        return $this->unite_colisage;
    }

    /**
     * Setter pour l'unité de colisage
     *
     * @param string $unite_colisage
     */
    public function setUniteColisage(string $unite_colisage)
    {
        $this->unite_colisage = $unite_colisage;
    }

    /**
     * Getter pour l'id de la tva
     *
     * @return int
     */
    public function getIdTva()
    {
        return (int) $this->id_tva;
    }

    /**
     * Setter pour l'id de la tva
     *
     * @param int $id_tva
     */
    public function setIdTva(int $id_tva)
    {
        $this->id_tva = $id_tva;
    }
}
