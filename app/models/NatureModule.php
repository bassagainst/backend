<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;

/**
 * Class NatureModule
 * @package App\Model
 */
class NatureModule extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var string $nom */
    protected $nom;

    /** @var double $hauteur */
    protected $hauteur;

    /** @var double $longueur */
    protected $longueur;

    /** @var string $unite */
    protected $unite;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Nature_Module");

        $this->hasMany('id', 'App\Model\Module', 'id_nature',
            [
                'alias'      => 'Module',
                'foreignKey' => true
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Nature_Module';
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Getter pour la hauteur
     *
     * @return float
     */
    public function getHauteur()
    {
        return (float) $this->hauteur;
    }

    /**
     * Setter pour la hauteur
     *
     * @param float $hauteur
     */
    public function setHauteur(float $hauteur)
    {
        $this->hauteur = $hauteur;
    }

    /**
     * Getter pour la longueur
     *
     * @return float
     */
    public function getLongueur()
    {
        return (float) $this->longueur;
    }

    /**
     * Setter pour la longueur
     *
     * @param float $longueur
     */
    public function setLongueur(float $longueur)
    {
        $this->longueur = $longueur;
    }

    /**
     * Getter pour l'unité
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Setter pour l'unité
     *
     * @param string $unite
     */
    public function setUnite(string $unite)
    {
        $this->unite = $unite;
    }
}
