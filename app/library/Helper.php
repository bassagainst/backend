<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library;

use App\Model\Historique;
use App\Model\ResourcesAccesses;
use App\Model\Utilisateur;
use DateTime;
use Firebase\JWT\JWT;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Phalcon\Acl\AdapterInterface;
use Phalcon\Di;
use Phalcon\Filter;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\Model\MessageInterface;
use Phalcon\Security\Exception;
use Phalcon\Security\Random;

/**
 * Class Helper
 * @package App\Library
 */
class Helper extends Component
{
    /**
     * Helper constructor.
     */
    private function __construct()
    {
    }

    /**
     * Retourne la liste des acls d'une resource
     *
     * @param $model
     * @return array
     * @throws \ReflectionException
     */
    public static function searchAcls($model)
    {
        /** @var AdapterInterface $acl */
        $acl = Di::getDefault()->getShared('acl');

        $class = new \ReflectionClass($model);

        $accesses = ResourcesAccesses::find(
            [
                'conditions' => 'resources_name = ?1',
                'bind' => [
                    1 => $class->getShortName()
                ]
            ]
        );

        if ($accesses !== false) {

            $session = self::getTokenSession();

            $resultat = [];

            /** @var ResourcesAccesses $access */
            foreach ($accesses as $access) {

                $resultat[$access->access_name] = $acl->isAllowed(
                    $session->role,
                    $class->getShortName(),
                    $access->access_name
                );
            }

            return $resultat;
        }

        return [];
    }

    /**
     * Génère une chaine d'identifiant pour le cache
     *
     * @param $source
     * @param $parameters
     * @return string
     */
    public static function createCacheKey($source, $parameters)
    {
        $uniqueKey = [];
        $uniqueKey[] = $source;

        foreach ($parameters as $key => $value) {

            if (is_scalar($value)) {

                $uniqueKey[] = $key . ':' . $value;

            } elseif (is_array($value)) {

                $uniqueKey[] = $key . ':[' . self::createCacheKey($source, $value) . ']';
            }
        }

        return '_PHCM_DC' . join(',', $uniqueKey);
    }

    /**
     * Méthode de validation et de formattage d'une date
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Méthode de formattage du téléphone pour l'affichage
     *
     * @param $tel
     * @return null|string
     * @throws NumberParseException
     */
    public static function formatTelephone($tel)
    {
        $format = null;

        if (is_string($tel) && !empty($tel)) {

            $libPhone = PhoneNumberUtil::getInstance();

            $parsedNumber = $libPhone->parse($tel, 'FR');

            $format = $libPhone->format($parsedNumber, PhoneNumberFormat::NATIONAL);
        }

        return $format;
    }

    /**
     * Méthode de formattage pour le stockage des numéros de téléphones
     *
     * @param $tel
     * @return string
     * @throws \Exception
     */
    public static function storeTelephone($tel)
    {
        $store = '';

        if (is_string($tel)) {

            $libPhone = PhoneNumberUtil::getInstance();

            try {

                $parsedNumber = $libPhone->parse($tel, 'FR');

                $store = $libPhone->format($parsedNumber, PhoneNumberFormat::INTERNATIONAL);

            } catch (NumberParseException $e) {

                throw new \Exception("Le numéro de téléphone n'est pas valide : " . $e->getMessage() . ' !');
            }
        }

        return $store;
    }

    /**
     * Méthode de génération d'un UUID
     *
     * @param Utilisateur $utilisateur
     * @param int $expire
     * @return string
     * @throws \Exception
     */
    public static function generateUuid(Utilisateur $utilisateur, $expire)
    {
        /** @var \Redis $redis */
        $redis = Di::getDefault()->getShared('redis');

        /** @var \Phalcon\Http\Request $request */
        $request = Di::getDefault()->getShared('request');

        $uuid = self::randomUuid();

        $payload = [
            'at' => (new DateTime)->getTimestamp(),
            'userId' => $utilisateur->getId(),
            'userEmail' => $utilisateur->getEmail(),
            'userAgent' => $request->getUserAgent(),
            'ipAddress' => $request->getClientAddress(true) ?? 'unknown'
        ];

        if (!$redis->set($uuid, json_encode($payload), $expire)) {

            throw new \Exception("Le serveur n'a pas pu générer le jeton Uuid");
        }

        return $uuid;
    }

    /**
     * Méthode d'encodage JWT préconfiguré
     *
     * @param array $data
     * @return string
     */
    public static function encodeJwt(array $data): string
    {
        /** @var \Phalcon\Config $security */
        $security = Di::getDefault()->getShared('config')->get('security');

        $key = (is_object($security->get('jwtKey'))
            || is_array($security->get('jwtKey')))
            ? $security->get('jwtKey')->get('private')
            : $security->get('jwtKey');

        return JWT::encode($data, $key, $security->get('jwtType'));
    }

    /**
     * Méthode de décodage JWT préconfiguré
     *
     * @param string $token
     * @return object
     */
    public static function decodeJwt(string $token)
    {
        /** @var \Phalcon\Config $security */
        $security = Di::getDefault()->getShared('config')->get('security');

        $key = (is_object($security->get('jwtKey'))
            || is_array($security->get('jwtKey')))
            ? $security->get('jwtKey')->get('public')
            : $security->get('jwtKey');

        return JWT::decode($token, $key, $security->get('jwtAlg')->toArray());
    }

    /**
     * Helper de simplification de retour des messages
     *
     * @param Model $model
     * @return array
     */
    public static function getMessages(Model $model)
    {
        $errors = [];

        /** @var MessageInterface $message */
        foreach ($model->getMessages() as $message) {

            $errors[$message->getField()] = $message->getMessage();
        }

        return $errors;
    }

    /**
     * Méthode de génération d'un Uuid
     *
     * @return string
     */
    private static function randomUuid()
    {
        $random = new Random;

        try {

            return $random->uuid();

        } catch (Exception $e) {

            return self::randomUuid();
        }
    }

    /**
     * @param string $crud
     * @param int $id_utilisateur
     * @param string $table
     * @param int $id_table
     * @throws \Exception
     */
    public static function setHistorique(string $crud, int $id_utilisateur, string $table, int $id_table)
    {
        $historique = new Historique();

        $historique->setCrud($crud);
        $historique->setIdUtilisateur($id_utilisateur);
        $historique->setTable($table);
        $historique->setIdTable($id_table);

        if ($historique->save() === false) {

            throw new \Exception("Impossible d'écrire l'historique de l'application !");
        }
    }

    /**
     * Retourne le token de session utilisateur
     *
     * @return bool|object
     */
    public static function getTokenSession()
    {
        $request = Di::getDefault()->get('request');

        $authorizationHeader = $request->getHeader('Authorization');

        if (PHP_SAPI === 'cli') {

            $payload = new \stdClass;
            $payload->id_utilisateur = $root['id_utilisateur'] ?? 0;
            $payload->nom = $root['nom'] ?? 'Admin';
            $payload->prenom = $root['prenom'] ?? 'Admin';
            $payload->role = $root['role'] ?? null;

            return $payload;
        }

        $switch = substr($authorizationHeader, 0, strpos($authorizationHeader, ' '));

        switch ($switch) {
            case 'Basic':

                $filter = new Filter;
                $credentials = $request->getBasicAuth();

                $email = $filter->sanitize($credentials['username'], Filter::FILTER_EMAIL);
                $mdp = $filter->sanitize($credentials['password'], Filter::FILTER_STRIPTAGS);

                $utilisateur = Utilisateur::findFirst(
                    [
                        'conditions' => 'email = ?1',
                        'bind'       => [
                            1 => $email
                        ]
                    ]
                );

                if ($utilisateur !== false
                    && $utilisateur instanceof Utilisateur
                    && $utilisateur->checkMdp($mdp)) {

                    $payload = new \stdClass;

                    $payload->id_utilisateur = $utilisateur->getId();
                    $payload->email = $utilisateur->getEmail();
                    $payload->nom = $utilisateur->getNom();
                    $payload->prenom = $utilisateur->getPrenom();
                    $payload->role = $utilisateur->getRole();

                    return $payload;
                }


                break;

            case 'Bearer':

                if (preg_match('/Bearer\s+(.*)$/i', $authorizationHeader, $matches)) {

                    try {

                        $data = Helper::decodeJwt($matches[1]);

                        return $data->payload;

                    } catch (\Exception $e) {

                        return false;
                    }
                }

                break;
        }

        return false;
    }
}
