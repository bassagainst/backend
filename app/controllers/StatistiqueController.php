<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Controller;

use App\Library\Controller\ControllerBase;
use App\Library\Plugin\SecurityPlugin;
use App\Model\Client;
use App\Model\Devis;
use App\Model\Fournisseur;
use App\Model\LiaisonModuleComposant;
use App\Model\Utilisateur;

/**
 * Class StatistiqueController
 * @package App\Controller
 */
class StatistiqueController extends ControllerBase
{
    public function beforeExecuteRoute()
    {
        $security = new SecurityPlugin;

        if ($security->authenticate()) {

            return true;

        } else {

            $this->response->setStatusCode(401, 'Unauthorized');

            $this->response->setJsonContent(
                [
                    'status' => 'FAILED',
                    'message' => "L'authentification à échouée ou expirée !"
                ]
            );

            return false;
        }
    }

    public function settingsAction()
    {

    }

    public function conceptionAction()
    {

    }

    public function adminAction()
    {
        $utilisateurs = Utilisateur::count(
            [
                'conditions' => 'deleted = 0'
            ]
        );

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'OK',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => [
                'utilisateurs' => $utilisateurs ?? 0,
                'mysql' => $this->mysqlAction()['data']['queries_per_second_avg'] ?? 0,
                'redis' => $this->redisAction()['data']['memory']['used_memory_human'] ?? 0,
                'php' => $this->phpAction()['data']['used_memory_human'] ?? 0
            ]
        ];
    }

    public function gestionAction()
    {

    }

    /**
     * Affiche les statistiques du dashboard
     *
     * @return array
     * @throws \Exception
     */
    public function dashboardAction()
    {
        $clients = Client::count(
            [
                'conditions' => 'deleted = 0'
            ]
        );

        $fournisseurs = Fournisseur::count(
            [
                'conditions' => 'deleted = 0'
            ]
        );

        /** @var Devis[] $devisList */
        $devisList = Devis::find(
            [
                'conditions' => 'deleted = 0'
            ]
        );

        $devisTotal = 0;

        foreach ($devisList as $devis) {

            foreach ($devis->getModule() as $module) {

                if ($module->isDeleted()) continue;

                foreach ($module->getLiaisonModuleComposant() as $k => $composant) {

                    if ($composant->isDeleted()) continue;

                    /** @var LiaisonModuleComposant $extraObject */
                    $extraObject = $module->getRelated('LiaisonModuleComposant', [
                        'columns' => LiaisonModuleComposant::class . '.quantite'
                    ])[$k];

                    $extraArray = $extraObject->toArray(['quantite']);

                    $composantArray = $composant->toArray(['prix_ht']);

                    $priceHt = $composantArray['prix_ht'] * $extraArray['quantite'];

                    $devisTotal += $priceHt;
                }
            }
        }

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'OK',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => [
                'clients' => $clients ?? 0,
                'fournisseurs' => $fournisseurs ?? 0,
                'devis_prix_ht' => (float)number_format((float)$devisTotal, 2, '.', '') ?? 0,
                'facture_prix_ht' => 0
            ]
        ];
    }

    /**
     * Retourne les statistiques sur la base MySQL
     *
     * @return array
     */
    public function mysqlAction()
    {
        $pdo = $this->db->getInternalHandler();

        /** @noinspection PhpComposerExtensionStubsInspection */
        $serverInfo = explode('  ', $pdo->getAttribute(\PDO::ATTR_SERVER_INFO));

        $dataInfo = [];

        foreach ($serverInfo as $si) {

            $expsi = explode(': ', $si);
            $key = str_replace(' ', '_', strtolower($expsi[0]));
            $value = (floatval($expsi[1]) == intval($expsi[1]))
                ? intval($expsi[1])
                : floatval($expsi[1]);

            $dataInfo[$key] = $value;
        }

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'OK',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => $dataInfo
        ];
    }

    /**
     * Retourne les statistiques sur la base Redis
     *
     * @return array
     */
    public function redisAction()
    {
        /** @var \Redis $redis */
        $redis = $this->getDI()->getShared('redis');

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'OK',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => [
                'cpu' => $redis->info('cpu'),
                'memory' => $redis->info('memory'),
                'clients' => $redis->info('clients'),
                'stats' => $redis->info('stats')
            ]
        ];
    }

    public function phpAction()
    {
        $r = [];
        $unit=array('B','KB','MB','GB','TB','PB');
        $r['used_memory'] = memory_get_usage();
        $r['used_memory_human'] = @round($r['used_memory']/pow(1000,($i=floor(log($r['used_memory'],1000)))),2)
            . (isset($unit[$i]) ? $unit[$i] : 'B');
        $r['maxmemory'] = memory_get_peak_usage(true);
        $r['maxmemory_human'] = @round($r['maxmemory']/pow(1000,($i=floor(log($r['maxmemory'],1000)))),2)
            . (isset($unit[$i]) ? $unit[$i] : 'B');

        $requestTime = $this->request->getServer('REQUEST_TIME_FLOAT') ?? STARTTIME;

        return [
            'status' => 'OK',
            'latency' => round((microtime(true) - $requestTime) * 1000),
            'data' => $r
        ];
    }
}

