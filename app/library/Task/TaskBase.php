<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Task;

use Phalcon\Cli\Task;

/**
 * Class TaskBase
 *
 * Classe de base pour les tâches consoles
 *
 * @package App\Library\Task
 */
class TaskBase extends Task
{
    const TAG = <<<EOD
  ____                                  _           _    ____ _ _ 
 | __ )  __ _ ___ ___  __ _  __ _  __ _(_)_ __  ___| |_ / ___| (_)
 |  _ \ / _` / __/ __|/ _` |/ _` |/ _` | | '_ \/ __| __| |   | | |
 | |_) | (_| \__ \__ \ (_| | (_| | (_| | | | | \__ \ |_| |___| | |
 |____/ \__,_|___/___/\__,_|\__, |\__,_|_|_| |_|___/\__|\____|_|_|
                            |___/                                 
EOD;

    const OPEN_COLOR = "\033[";

    const STOP_COLOR = "\033[0m";

    const FOREGROUND_COLORS = [
        'black'        => '0;30m',
        'dark_gray'    => '1;30m',
        'blue'         => '0;34m',
        'light_blue'   => '1;34m',
        'green'        => '0;32m',
        'light_green'  => '1;32m',
        'cyan'         => '0;36m',
        'light_cyan'   => '1;36m',
        'red'          => '0;31m',
        'light_red'    => '1;31m',
        'purple'       => '0;35m',
        'light_purple' => '1;35m',
        'brown'        => '0;33m',
        'yellow'       => '1;33m',
        'light_gray'   => '0;37m',
        'white'        => '1;37m'
    ];

    const BACKGROUND_COLORS = [
        'black'      => '40m',
        'red'        => '41m',
        'green'      => '42m',
        'yellow'     => '43m',
        'blue'       => '44m',
        'magenta'    => '45m',
        'cyan'       => '46m',
        'light_gray' => '47m'
    ];

    /**
     * Affiche le nom de la console au lancement d'une commande
     */
    public function onConstruct()
    {
        self::nl(self::TAG . PHP_EOL, 'light_green');
    }

    /**
     * Place une ligne vide à la fin de la console
     */
    public function __destruct()
    {
        self::nl();
    }

    /**
     * Liste les méthodes disponnibles d'une tâche
     *
     * @param $class
     */
    public static function listMethods($class)
    {
        try {

            $actions = (new \ReflectionClass($class))->getMethods(\ReflectionMethod::IS_PUBLIC);

        } catch (\ReflectionException $e) {

            $actions = [];
        }

        foreach ($actions as $action) {

            if (strpos($action->getName(), 'Action') !== false) {

                if ($action->getName() !== 'mainAction') {

                    self::nl('- ' . strtolower(substr($action->getName(), 0, -6)));
                }
            }
        }
    }

    /**
     * Attend une entrée utilisateur et la retourne
     *
     * @param bool $hide
     * @return string
     */
    public static function fgets($hide = false)
    {
        if ($hide) shell_exec('stty -echo');

        $stdin = trim(fgets(STDIN));

        if ($hide) shell_exec('stty echo');

        return $stdin;
    }

    /**
     * Affiche une nouvelle ligne dans le terminal de la console
     *
     * @param string $text
     * @param null $fgColor
     * @param null $bgColor
     * @param bool $nl
     * @return string
     */
    public static function nl(string $text = '', $fgColor = null, $bgColor = null, $nl = true)
    {
        $coloredString = '';

        // Définit si une couleur de texte sera implémentée
        if (isset(self::FOREGROUND_COLORS[$fgColor])) {

            $coloredString .= self::OPEN_COLOR . self::FOREGROUND_COLORS[$fgColor];
        }

        // Définit si une couleur de fond sera implémentée
        if (isset(self::BACKGROUND_COLORS[$bgColor])) {

            $coloredString .= self::OPEN_COLOR . self::BACKGROUND_COLORS[$bgColor];
        }

        // Compose la chaine avec le texte
        $coloredString .= $text . self::STOP_COLOR;

        if ($nl) $coloredString .= PHP_EOL;

        // Affiche la chaine dans la console
        print $coloredString;
    }
}
