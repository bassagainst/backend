<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Model;

use App\Library\Model\ModelBase;
use Phalcon\Builder\Project\Simple;

/**
 * Class Module
 * @package App\Model
 *
 * @property Simple|Gamme[]                  $gamme
 * @property Simple|Devis[]                  $devis
 * @property Simple|NatureModule[]           $natureModule
 * @property Simple|LiaisonModuleComposant[] $liaisonModuleComposant
 *
 * @method   Simple|Gamme[]                  getGamme($parameters = null)
 * @method   Simple|Devis[]                  getDevis($parameters = null)
 * @method   Simple|NatureModule[]           getNatureModule($parameters = null)
 * @method   Simple|LiaisonModuleComposant[] getLiaisonModuleComposant($parameters = null)
 *
 * @method   integer                         countGamme()
 * @method   integer                         countDevis()
 * @method   integer                         countNatureModule()
 * @method   integer                         countLiaisonModuleComposant()
 */
class Module extends ModelBase
{
    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = true;

    /** @var integer $id_gamme */
    protected $id_gamme;

    /** @var integer $id_devis */
    protected $id_devis;

    /** @var integer $id_nature */
    protected $id_nature;

    /** @var string $nom */
    protected $nom;

    /** @var double $marge */
    protected $marge;

    /** @var boolean $description */
    protected $isModele;

    /** @var string $description */
    protected $description;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->setSource("Module");

        $this->hasMany('id', 'App\Model\LiaisonModuleComposant', 'id_module',
            [
                'alias' => 'LiaisonModuleComposant',
                'foreignKey' => true
            ]
        );

        $this->belongsTo('id_gamme', 'App\Model\Gamme', 'id',
            [
                'alias' => 'Gamme',
                'foreignKey' => [
                    'message' => "La gamme choisie n'existe pas"
                ]
            ]
        );

        $this->belongsTo('id_devis', 'App\Model\Devis', 'id',
            [
                'alias' => 'Devis',
                'foreignKey' => [
                    'message' => "Le devis choisi n'existe pas",
                    'allowNulls' => true
                ]
            ]
        );

        $this->belongsTo('id_nature', 'App\Model\NatureModule', 'id',
            [
                'alias' => 'NatureModule',
                'foreignKey' => [
                    'message' => "La nature de module choisie n'existe pas"
                ]
            ]
        );

        $this->hasManyToMany(
            "id",
            "App\Model\LiaisonModuleComposant",
            "id_module", "id_composant",
            "App\Model\Composant",
            "id",
            [
                'alias' => 'LiaisonModuleComposant'
            ]
        );

    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getSource()
    {
        return 'Module';
    }

    /**
     * Getter pour l'id de la gamme
     *
     * @return int
     */
    public function getIdGamme()
    {
        return (int)$this->id_gamme;
    }

    /**
     * Setter pour l'id de la gamme
     *
     * @param int $id_gamme
     */
    public function setIdGamme(int $id_gamme)
    {
        $this->id_gamme = $id_gamme;
    }

    /**
     * Getter pour l'id du devis
     *
     * @return int
     */
    public function getIdDevis()
    {
        return (int)$this->id_devis;
    }

    /**
     * Setter pour l'id du devis
     *
     * @param int $id_devis
     */
    public function setIdDevis(int $id_devis)
    {
        $this->id_devis = $id_devis;
    }

    /**
     * Getter pour l'id de la nature
     *
     * @return int
     */
    public function getIdNature()
    {
        return (int)$this->id_nature;
    }

    /**
     * Setter pour l'id de la nature
     *
     * @param int $id_nature
     */
    public function setIdNature(int $id_nature)
    {
        $this->id_nature = $id_nature;
    }

    /**
     * Getter pour le nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Setter pour le nom
     *
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Getter pour la marge
     *
     * @return double
     */
    public function getMarge()
    {
        return (double)$this->marge;
    }

    /**
     * Setter pour la marge
     *
     * @param float $marge
     */
    public function setMarge(float $marge)
    {
        $this->marge = $marge;
    }

    /**
     * Getter pour la vérification d'un module modèle
     *
     * @return bool
     */
    public function isModele()
    {
        return intval($this->isModele);
    }

    /**
     * Setter pour la transformation d'un module en modèle de module
     *
     * @param bool $isModele
     */
    public function setIsModele(bool $isModele)
    {
        $this->isModele = intval($isModele);
    }

    /**
     * Getter pour la description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter pour la description
     *
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
}
