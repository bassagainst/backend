<?php declare(strict_types=1);
/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

namespace App\Library\Model;

use App\Library\Helper;
use Exception;
use Phalcon\Mvc\Model;
use ReflectionClass;
use ReflectionProperty;

/**
 * @inheritdoc
 *
 * Class ModelBase
 *
 * Model de base implémentation des fonction communes à tous les models
 *
 * @package App\Library\Model
 */
abstract class ModelBase extends Model
{
    /** @var int TTL_CACHE */
    const TTL_CACHE = 300;

    /** @var int DELETED */
    const DELETED = 1;

    /** @var int NOT_DELETED */
    const NOT_DELETED = 0;

    /** @var bool ARCHIVABLE */
    const ARCHIVABLE = false;

    /**
     * @OA\Property(
     *     format="int64",
     *     description="Identifiant de l'entitée",
     *     title="id",
     * )
     *
     * @var int $id
     */
    protected $id;

    /** @var bool $deleted */
    protected $deleted;

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        // Méthode d'initialisation
    }

    /**
     * Ovverride de la méthode de suppression d'un modèle
     *
     * @return bool
     * @throws Exception
     */
    public function delete()
    {
        if ($this->getModelsMetaData()->hasAttribute($this, 'deleted')) {

            $this->setDeleted(true);

            $update = $this->save(null, null, false);

            $this->flush(get_called_class());

            if (get_called_class()::ARCHIVABLE) {

                $id_utilisateur = Helper::getTokenSession()->id_utilisateur;

                Helper::setHistorique(
                    'D',
                    intval($id_utilisateur),
                    $this->getSource(),
                    $this->getId()
                );
            }

            return $update;

        } else {

            return parent::delete();
        }
    }

    /**
     * Getter pour Id de base de données
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->id;
    }

    /**
     * Vérification d'un objet supprimé
     *
     * @return bool
     */
    public function isDeleted(): bool
    {
        return boolval($this->deleted);
    }

    /**
     *
     *
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = intval($deleted);
    }

    /**
     * Méthode toArray custom retournant les valeurs via leurs getters et setters
     *
     * @param null $columns
     * @return array
     * @throws Exception
     */
    public function toArray($columns = null)
    {
        $data = [];

        try {

            $class = new ReflectionClass($this);
            $properties = $class->getProperties(
                ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC
            );

            foreach($properties as $reflection) {

                $property = $reflection->getName();
                $method = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $property)));

                if ((empty($columns) || in_array($property, $columns))
                && in_array($property, $this->getModelsMetaData()->getAttributes($this))) {

                    $data[$property] = ($reflection->isProtected() && $class->hasMethod($method))
                        ? $this->$method()
                        : $this->$property;
                }
            }

            return $data;

        } catch (\ReflectionException $e) {

            throw new Exception("Une erreur est survenue lors d'un toArray : " . $e->getTraceAsString());
        }
    }

    /**
     * Méthode de recherche d'éléments (avec gestion du cache)
     *
     * @inheritdoc
     *
     * @param null $parameters
     * @return Model\ResultsetInterface
     */
    public static function find($parameters = null)
    {
        if (get_called_class()::TTL_CACHE !== -1) {

            if (!is_array($parameters)) {

                $parameters = [$parameters];
            }

            if (!isset($parameters['cache'])) {

                $parameters['cache'] = [
                    'key'      => Helper::createCacheKey('internal:' . get_called_class(), $parameters),
                    'lifetime' => get_called_class()::TTL_CACHE,
                ];
            }
        }

        return parent::find($parameters);
    }

    /**
     * Override de la méthode de sauvegarde
     *
     * @param null $data
     * @param null $whiteList
     * @param bool $archivable
     * @return bool
     * @throws Exception
     */
    public function save($data = null, $whiteList = null, $archivable = true)
    {
        $save = parent::save($data, $whiteList);

        $this->flush(get_called_class());

        if ($save && $archivable && get_called_class()::ARCHIVABLE) {

            $id_utilisateur = isset(Helper::getTokenSession()->id_utilisateur)
                ? Helper::getTokenSession()->id_utilisateur
                : 0;

            Helper::setHistorique(
                $this->getOPLabel(),
                intval($id_utilisateur),
                $this->getSource(),
                $this->getId()
            );
        }

        return $save;
    }

    /**
     * Méthode de getter magique
     *
     * @param $property
     * @return \Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset
     * @throws \ReflectionException
     * @throws Exception
     */
    public function __get($property)
    {
        $reflection = new ReflectionClass($this);

        if ($reflection->getProperty($property)->isPublic()) {

            return $this->$property;

        } else {

            $method = 'get' . ucfirst($property);

            if ($reflection->hasMethod($method)) {

                return $this->$method();

            } else {

                throw new Exception("Le getter <$method> n'existe pas !");
            }
        }
    }

    /**
     * Méthode de setter magique
     *
     * @param string $property
     * @param mixed $value
     * @throws \ReflectionException
     * @throws Exception
     */
    public function __set($property, $value)
    {
        $reflection = new ReflectionClass($this);

        if ($reflection->getProperty($property)->isPublic()) {

            $this->$property = $value;

        } else {

            $method = 'set' . ucfirst($property);

            if ($reflection->hasMethod($method)) {

                $this->$method($value);

            } else {

                throw new Exception("Le setter <$method> n'existe pas !");
            }
        }
    }

    /**
     * Méthode d'ovveride des retours par défaut du validateur de messages
     *
     * @param null $filter
     * @return array|Model\MessageInterface[]
     */
    public function getMessages($filter = null)
    {
        $messages = parent::getMessages();

        foreach ($messages as $message) {

            switch ($message->getField()) {
                case 'mdpOrigin':
                    $message->setField('mdp');
                    break;

                default:
                    break;
            }

            switch ($message->getType()) {
                case 'PresenceOf':
                    $message->setMessage('Le champ ' . $message->getField() . ' est requis');
                    break;

//                case 'ConstraintViolation':
//                    $message->setMessage("L'élément est verouillé par une ou des contraintes");
//                    break;

                default:
                    break;
            }
        }

        return $messages;
    }

    /**
     * Flush cache automatique
     * @param null $prefix
     */
    protected function flush($prefix = null)
    {
        if ($this->getDI()->has('modelsCache')) {

            /** @var \Phalcon\Cache\Backend\Redis $modelsCache */
            $modelsCache = $this->getDI()->get('modelsCache');

            if (is_null($prefix)) {

                $modelsCache->flush();

            } else {

//                $modelsCache->delete($prefix . '*');

                foreach ($modelsCache->queryKeys('_PHCM_DC' . 'internal:' . $prefix) as $key) {

                    $modelsCache->delete($key);
                }
            }
        }
    }

    /**
     * Récupère une char de l'action courante pour l'historisation
     * @return string
     */
    private function getOPLabel() {
        switch ($this->getOperationMade()) {
            case self::OP_CREATE :
                return 'C';
            case self::OP_DELETE :
                return 'D';
            case self::OP_UPDATE :
                return 'U';
        }
        return '';
    }
}
