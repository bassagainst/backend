define({
  "name": "Madera API",
  "version": "0.0.1",
  "description": "Documentation de l'API RESTFULL de l'application Madera",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-12-21T15:54:48.171Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
